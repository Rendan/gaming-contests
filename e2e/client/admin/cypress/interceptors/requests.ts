import { Alias } from "./aliases";

type InterceptedRequest = {
  method: string;
  url: string;
  alias: Alias;
};

export const requests: Array<InterceptedRequest> = [
  {
    method: "GET",
    url: "/users*",
    alias: Alias.USERS_LOAD,
  },
];
