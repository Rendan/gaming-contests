import { Alias } from "./aliases";
import { requests } from "./requests";

const BASE_URL = "api/v1";

export const interceptApiRequests = () => {
  requests.forEach(({ method, url, alias }) => {
    cy.intercept({
      method,
      url: `${BASE_URL}${url}`,
    }).as(String(alias));
  });
};

export const waitForApiRequest = (alias: Alias) => cy.wait(`@${alias}`);
