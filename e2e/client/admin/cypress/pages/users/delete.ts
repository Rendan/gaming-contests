import { Confirmation } from "blocks/confirm";

export class UserDeletePage {
  private confirmation = new Confirmation("delete-user-confirmation");

  deleteConfirm() {
    this.confirmation.confirm();
  }

  deleteCancel() {
    this.confirmation.cancel();
  }
}
