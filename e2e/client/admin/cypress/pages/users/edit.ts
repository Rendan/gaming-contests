import { Form } from "blocks/form";
import { User } from "types/user";

export class UserEditPage {
  private form = new Form<User>("user-edit-form", [
    { name: "email", type: "input" },
    { name: "firstName", type: "input" },
    { name: "lastName", type: "input" },
    { name: "status", type: "select" },
    { name: "role", type: "select" },
  ]);

  fillEmail(email: string) {
    this.form.setInputValue({ name: "email", value: email });

    return this;
  }

  fillFirstName(firstName: string) {
    this.form.setInputValue({ name: "firstName", value: firstName });

    return this;
  }

  fillLastName(lastName: string) {
    this.form.setInputValue({ name: "lastName", value: lastName });

    return this;
  }

  selectRole(role: string) {
    this.form.setSelectValue({ name: "role", value: role });

    return this;
  }

  selectStatus(status: string) {
    this.form.setSelectValue({ name: "status", value: status });

    return this;
  }

  submit() {
    this.form.submit();

    return this;
  }
}
