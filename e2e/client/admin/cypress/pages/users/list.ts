import { Form } from "blocks/form";
import { Table } from "blocks/table";
import { User } from "types/user";

export class UsersListPage {
  public table = new Table<User>("users-list");
  private searchForm = new Form<User>("users-list-search-form", [
    { name: "id", type: "input" },
    { name: "email", type: "input" },
    { name: "firstName", type: "input" },
    { name: "lastName", type: "input" },
    { name: "status", type: "select" },
    { name: "role", type: "select" },
  ]);

  search(values: User) {
    this.searchForm.setValues(values);

    return this;
  }

  clickOnUserDetailsLink(rowIndex: number, field: keyof User) {
    this.table.getTableCellLink(rowIndex, field).click();

    return this;
  }
}
