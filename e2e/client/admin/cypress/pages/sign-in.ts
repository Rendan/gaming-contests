type UserCredentials = {
  email: string;
  password: string;
};

export class SignInPage {
  signIn(userCredentials: UserCredentials) {
    cy.get("[data-cy='email']").type(userCredentials.email);
    cy.get("[data-cy='password']").type(userCredentials.password);

    cy.get("[data-cy='sign-in']").submit();

    return this;
  }

  signInAsAdmin() {
    this.signIn({ email: "john.doe@gmail.com", password: "password" });

    return this;
  }
}
