export const isKeyOf = <T>(
  name: string | number | symbol,
  obj: T
): name is keyof T => {
  return name in obj ? true : false;
};
