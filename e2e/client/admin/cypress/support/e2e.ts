import { interceptApiRequests } from "interceptors";

before(() => {
  cy.exec("sh create-db-dump.sh");
});

beforeEach(() => {
  interceptApiRequests();

  cy.exec("sh import-db-dump.sh");
  cy.visit("/");
});

after(() => {
  cy.exec("sh import-db-dump.sh");
});
