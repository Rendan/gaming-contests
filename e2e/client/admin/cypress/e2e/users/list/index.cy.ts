import {
  SEARCH_TEST_CASE_DATA_COLLECTION,
  SORTING_TEST_CASE_DATA_COLLECTION,
  DROPDOWN_ACTIONS_TEST_CASE_DATA_COLLECTION,
  USER_DETAILS_LINKS_TEST_CASE_DATA_COLLECTION,
} from "./constants";

import {
  prepareTestCase,
  checkUsersSearch,
  checkInfiniteScroll,
  checkUserDetailsLinks,
  checkUsersListSorting,
  checkLinksInDropdownActions,
  checkUserEditing,
  checkDeletingUser,
} from "./test-cases";

describe("Users List Page", () => {
  beforeEach(prepareTestCase);

  describe("Search functionality", () => {
    it("should work with every search criteria", () => {
      SEARCH_TEST_CASE_DATA_COLLECTION.map(checkUsersSearch);
    });
  });

  describe("Sorting", () => {
    it("should work by clicking on every table heading in both sorting directions", () => {
      SORTING_TEST_CASE_DATA_COLLECTION.map(checkUsersListSorting);
    });
  });

  describe("Infinite scroll", () => {
    it("should work when scrolling all the way to the last user", () => {
      checkInfiniteScroll();
    });
  });

  describe("Navigation", () => {
    it("should work when clicking on the user details links inside table", () => {
      USER_DETAILS_LINKS_TEST_CASE_DATA_COLLECTION.map(checkUserDetailsLinks);
    });

    it("should work when clicking on the dropdown actions", () => {
      DROPDOWN_ACTIONS_TEST_CASE_DATA_COLLECTION.map(
        checkLinksInDropdownActions
      );
    });
  });

  describe("User Editing", () => {
    it("should work when clicking Edit button in the dropdown actions", () => {
      checkUserEditing();
    });
  });

  describe("Deleting a user", () => {
    it("should work when clicking Edit button in the dropdown actions", () => {
      checkDeletingUser();
    });
  });
});
