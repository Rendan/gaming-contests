import { User } from "types/user";
import {
  DropdownActionsTestCaseData,
  SearchTestCaseData,
  SortingTestCaseData,
} from "./test-cases";

export const SORTING_TEST_CASE_DATA_COLLECTION: Array<SortingTestCaseData> = [
  {
    field: "id",
    firstExpectedResult: "1",
    secondExpectedResult: "61",
  },
  {
    field: "email",
    firstExpectedResult: "Yvonne_Hoeger8@yahoo.com",
    secondExpectedResult: "Angus_Dicki@gmail.com",
  },
  {
    field: "firstName",
    firstExpectedResult: "Abigail",
    secondExpectedResult: "Winifred",
  },
  {
    field: "lastName",
    firstExpectedResult: "Witting",
    secondExpectedResult: "Ankunding",
  },
  {
    field: "status",
    firstExpectedResult: "Active",
    secondExpectedResult: "Disabled",
  },
  {
    field: "role",
    firstExpectedResult: "Guest",
    secondExpectedResult: "Admin",
  },
];

export const USER_DETAILS_LINKS_TEST_CASE_DATA_COLLECTION: Array<keyof User> = [
  "id",
  "email",
  "firstName",
  "lastName",
  "status",
  "role",
];

export const DROPDOWN_ACTIONS_TEST_CASE_DATA_COLLECTION: Array<DropdownActionsTestCaseData> =
  [
    {
      targetAction: "View",
      expectedLocation: "/users/61",
      expectedContent: "Grady Koch",
    },
    {
      targetAction: "Edit",
      expectedLocation: "/users/61/edit",
      expectedContent: "Update User",
    },
    {
      targetAction: "Delete",
      expectedLocation: "/users/61/delete",
      expectedContent: "Delete User",
    },
  ];

export const SEARCH_TEST_CASE_DATA_COLLECTION: Array<SearchTestCaseData> = [
  {
    searchValues: {
      id: "9999",
      email: "not.existing@gmail.com",
      firstName: "Not",
      lastName: "Existing",
      status: "active",
      role: "admin",
    },
    rowsCountExpected: 1,
    contentExpected: "There are no records",
  },
  {
    searchValues: {
      id: "",
      email: "gmail.com",
      firstName: "",
      lastName: "",
      status: "",
      role: "",
    },
    rowsCountExpected: 17,
    contentExpected: "Roslyn.King87@gmail.com",
  },
  {
    searchValues: {
      id: "1",
      email: "john.doe@gmail.com",
      firstName: "John",
      lastName: "Doe",
      status: "active",
      role: "admin",
    },
    rowsCountExpected: 1,
    contentExpected: "john.doe@gmail.com",
  },
];

export const USER_EDIT_STATUS_VALUE = "Disabled";
export const USER_EDIT_ROLE_VALUE = "Guest";

export const USER_EDIT_VALUES: Required<Omit<User, "id">> = {
  email: "george.gilman@hotmail.com",
  firstName: "George",
  lastName: "Gilman",
  status: USER_EDIT_STATUS_VALUE.toLowerCase(),
  role: USER_EDIT_ROLE_VALUE.toLowerCase(),
};
