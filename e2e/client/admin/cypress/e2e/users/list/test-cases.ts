import { waitForApiRequest } from "interceptors";
import { Alias } from "interceptors/aliases";
import { SignInPage } from "pages/sign-in";
import { UserDeletePage } from "pages/users/delete";
import { UserEditPage } from "pages/users/edit";
import { UsersListPage } from "pages/users/list";
import { User } from "types/user";
import {
  USER_EDIT_ROLE_VALUE,
  USER_EDIT_STATUS_VALUE,
  USER_EDIT_VALUES,
} from "./constants";

export type UserEditingTestCaseData = {
  rowIndex: number;
  values?: Record<keyof User, string>;
};

export type SortingTestCaseData = {
  field: keyof User;
  firstExpectedResult: string;
  secondExpectedResult: string;
};

export type DropdownActionsTestCaseData = {
  targetAction: "View" | "Edit" | "Delete";
  expectedLocation: string;
  expectedContent: string;
};

export type SearchTestCaseData = {
  searchValues: Record<keyof User, string>;
  rowsCountExpected: number;
  contentExpected: string;
};

const signInPage = new SignInPage();
const usersListPage = new UsersListPage();
const userEditPage = new UserEditPage();
const userDeletePage = new UserDeletePage();

const waitForUsersToLoad = () => {
  waitForApiRequest(Alias.USERS_LOAD);
};

export const prepareTestCase = () => {
  signInPage.signInAsAdmin();

  waitForUsersToLoad();
};

export const checkInfiniteScroll = () => {
  cy.contains("Carmen_Schulist@yahoo.com");

  cy.scrollTo("bottom");

  waitForUsersToLoad();

  cy.contains("Wilson.Ullrich@hotmail.com");

  cy.scrollTo("bottom");

  waitForUsersToLoad();

  cy.contains("john.doe@gmail.com");
};

export const checkUsersListSorting = (data: SortingTestCaseData) => {
  usersListPage.table.setSortingField(data.field);

  waitForUsersToLoad();

  usersListPage.table
    .getTableCell(0, data.field)
    .should("contain.text", data.firstExpectedResult);

  usersListPage.table.toggleSortingDirection();

  waitForUsersToLoad();

  usersListPage.table
    .getTableCell(0, data.field)
    .should("contain.text", data.secondExpectedResult);
};

export const checkUserDetailsLinks = (field: keyof User) => {
  usersListPage.clickOnUserDetailsLink(0, field);

  cy.location().should("have.property", "pathname", "/users/61");
  cy.contains("Grady Koch");

  cy.go(-1);

  waitForUsersToLoad();
};

export const checkLinksInDropdownActions = (
  data: DropdownActionsTestCaseData
) => {
  usersListPage.table.clickOnDropdownAction(0, data.targetAction);

  cy.location().should("have.property", "pathname", data.expectedLocation);
  cy.contains(data.expectedContent);

  cy.go(-1);

  waitForUsersToLoad();
};

export const checkUsersSearch = (data: SearchTestCaseData) => {
  usersListPage.search(data.searchValues);

  waitForUsersToLoad();

  usersListPage.table.getRows().should("have.length", data.rowsCountExpected);
  usersListPage.table.getRows().contains(data.contentExpected);
};

export const checkUserEditing = () => {
  usersListPage.table.getRow(0).contains("Tomas.Walsh@hotmail.com");

  usersListPage.table.clickOnDropdownAction(0, "Edit");

  userEditPage
    .fillEmail(USER_EDIT_VALUES.email)
    .fillFirstName(USER_EDIT_VALUES.firstName)
    .fillLastName(USER_EDIT_VALUES.lastName)
    .selectRole(USER_EDIT_VALUES.role)
    .selectStatus(USER_EDIT_VALUES.status)
    .submit();

  usersListPage.table.getRow(0).contains(USER_EDIT_VALUES.email);
  usersListPage.table.getRow(0).contains(USER_EDIT_VALUES.firstName);
  usersListPage.table.getRow(0).contains(USER_EDIT_VALUES.lastName);
  usersListPage.table.getRow(0).contains(USER_EDIT_STATUS_VALUE);
  usersListPage.table.getRow(0).contains(USER_EDIT_ROLE_VALUE);
};

export const checkDeletingUser = () => {
  usersListPage.table.getRow(0).contains("Tomas.Walsh@hotmail.com");

  usersListPage.table.clickOnDropdownAction(0, "Delete");

  userDeletePage.deleteConfirm();

  waitForUsersToLoad();

  usersListPage.table.getRows().should("have.length.above", 0);
  usersListPage.table
    .getRow(0)
    .should("not.contain", "Tomas.Walsh@hotmail.com");
};
