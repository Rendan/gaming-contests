import { Theme } from "blocks/theme";
import { waitForApiRequest } from "interceptors";
import { Alias } from "interceptors/aliases";
import { SignInPage } from "pages/sign-in";

describe("Theme switching", () => {
  const signInPage = new SignInPage();
  const theme = new Theme();

  const checkThemeToggle = () => {
    theme.getThemeContainer().should("have.class", "light");

    theme.themeSwitchButtonClick();

    theme.getThemeContainer().should("have.class", "dark");

    theme.themeSwitchButtonClick();

    theme.getThemeContainer().should("have.class", "light");
  };

  it("Theme switching working on the sign in page", () => {
    checkThemeToggle();
  });

  it("Theme switching working on the dashboard", () => {
    signInPage.signInAsAdmin();

    waitForApiRequest(Alias.USERS_LOAD);

    checkThemeToggle();
  });
});
