export class Theme {
  themeSwitchButtonClick() {
    cy.get<HTMLButtonElement>('[data-cy="theme-switch-button"]').click();
  }

  getThemeContainer() {
    return cy.get<HTMLDivElement>('[data-cy="theme-container"]');
  }
}
