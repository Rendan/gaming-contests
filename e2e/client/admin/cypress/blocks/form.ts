import { isKeyOf } from "utils/ts";

type Field<T> = { name: keyof T; type: "input" | "select" };
type Fields<T> = Array<Field<T>>;

export class Form<T> {
  name: string;
  fields: Fields<T>;

  constructor(name: string, fields: Fields<T>) {
    this.name = name;
    this.fields = fields;
  }

  get form() {
    return cy.get<HTMLFormElement>(`form[data-cy="${this.name}"]`);
  }

  getInput(name: keyof T) {
    return this.form.find<HTMLInputElement>(`input[data-cy="${String(name)}"]`);
  }

  getSelect(name: keyof T) {
    return this.form.find<HTMLSelectElement>(
      `select[data-cy="${String(name)}"]`
    );
  }

  setInputValue({
    name,
    value,
    shouldClear = true,
  }: {
    name: keyof T;
    value?: string;
    shouldClear?: boolean;
  }) {
    if (shouldClear) {
      this.getInput(name).clear();
    }

    if (value) {
      this.getInput(name).type(value);
    }

    return this;
  }

  setSelectValue({ name, value }: { name: keyof T; value?: string }) {
    this.getSelect(name).select(value || "");

    return this;
  }

  setValues(values: Partial<Record<keyof T, string>>) {
    const keys = Object.keys(values);

    keys.forEach((key) => {
      const targetField = this.fields.find(({ name }) => name === key);

      if (!targetField || !isKeyOf(key, values)) {
        return;
      }

      if (targetField.type === "input") {
        this.setInputValue({ name: key, value: values[key] });
      }

      if (targetField.type === "select") {
        this.setSelectValue({ name: key, value: values[key] });
      }
    });

    return this;
  }

  submit() {
    this.form.find('[data-cy="submit-button"]').click();

    return this;
  }
}
