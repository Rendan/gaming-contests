export class Confirmation {
  name: string;

  constructor(name: string) {
    this.name = name;
  }

  get root() {
    return cy.get<HTMLDivElement>(`[data-cy="${this.name}"]`);
  }

  cancel() {
    this.root
      .find<HTMLButtonElement>(
        '[data-cy="confirmation-body"] [data-cy="cancel"]'
      )
      .click();
  }

  confirm() {
    this.root
      .find<HTMLButtonElement>(
        '[data-cy="confirmation-body"] [data-cy="confirm"]'
      )
      .click();
  }
}
