export class Table<T> {
  name: string;

  constructor(name: string) {
    this.name = name;
  }

  get root() {
    return cy.get(`[data-cy="${this.name}"]`);
  }

  getChangeSortingButton(name: keyof T) {
    return this.root.find<HTMLButtonElement>(
      `[data-cy="change-sorting-button"][data-name="${String(name)}"]`
    );
  }

  getActiveSortingButton() {
    return this.root.find<HTMLButtonElement>(
      '[data-cy="change-sorting-button"][data-selected="true"]'
    );
  }

  setSortingField(name: keyof T) {
    this.getChangeSortingButton(name).click();

    return this;
  }

  toggleSortingDirection() {
    this.getActiveSortingButton().click();

    return this;
  }

  getRows() {
    return this.root.find('[data-cy="row"]');
  }

  getRow(index: number) {
    return this.getRows().eq(index);
  }

  getTableCell(rowIndex: number, name: keyof T) {
    return this.getRows()
      .eq(rowIndex)
      .find(`[data-cy="cell"][data-field="${String(name)}"]`);
  }

  getTableCellLink(rowIndex: number, name: keyof T) {
    return this.getTableCell(rowIndex, name).find('[data-cy="link"]');
  }

  getTableRowDropdownActionsToggleButton(rowIndex: number) {
    return this.getRows()
      .eq(rowIndex)
      .find(`[data-cy="dropdown-actions-toggle"]`);
  }

  getDropdownActionsList(rowIndex = 0) {
    return this.getRow(rowIndex).find('[data-cy="dropdown-actions"]');
  }

  getDropdownActionsToggleButton(rowIndex = 0) {
    return this.getRow(rowIndex).find('[data-cy="dropdown-actions-toggle"]');
  }

  clickOnDropdownActionsToggleButton(rowIndex = 0) {
    this.getDropdownActionsToggleButton(rowIndex).click();

    return this;
  }

  clickOnDropdownAction(rowIndex = 0, label: string) {
    this.clickOnDropdownActionsToggleButton(rowIndex);
    this.getDropdownActionsList(rowIndex)
      .find(`[data-cy="dropdown-action"][data-label="${label}"] > button`)
      .click();

    return this;
  }
}
