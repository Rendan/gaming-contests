import { defineConfig } from "cypress";

export const e2e = defineConfig({
  e2e: {
    baseUrl: process.env.CLIENT_ADMIN_E2E_BASE_URL,
    viewportWidth: 1920,
    viewportHeight: 1080,
  },
});
