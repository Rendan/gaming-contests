include .env

dev:
	docker-compose up

seed:
	docker-compose run --rm server-api bash -c "yarn run seed:refresh"
