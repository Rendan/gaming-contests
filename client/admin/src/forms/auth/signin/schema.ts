import { object } from 'yup';

import { requiredEmailField, requiredStringField } from 'utils/validation';

import { SignInForm } from './types';

import type { SchemaOf } from 'yup';

export const schema: SchemaOf<SignInForm> = object().shape({
  email: requiredEmailField(),
  password: requiredStringField(),
});
