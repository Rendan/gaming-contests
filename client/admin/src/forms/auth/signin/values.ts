import { SignInForm } from './types';

export const getInitialValues = (): SignInForm => ({
  email: '',
  password: '',
});
