import { SignUpPayload } from 'types/responses/api/v1/auth';

export type SignUpFormValues = SignUpPayload;
