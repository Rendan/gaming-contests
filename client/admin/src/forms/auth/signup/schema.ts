import { object } from 'yup';

import { requiredEmailField, requiredStringField } from 'utils/validation';

import { SignUpFormValues } from './types';

import type { SchemaOf } from 'yup';

export const schema: SchemaOf<SignUpFormValues> = object().shape({
  firstName: requiredStringField(),
  lastName: requiredStringField(),
  email: requiredEmailField(),
  password: requiredStringField(),
  passwordConfirmation: requiredStringField(),
});
