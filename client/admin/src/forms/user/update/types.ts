import { UserRaw } from 'types/responses/api/v1/user';

export type UserUpdateForm = Required<
  Pick<UserRaw, 'email' | 'firstName' | 'lastName' | 'status' | 'role'>
>;
