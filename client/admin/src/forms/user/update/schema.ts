import { object } from 'yup';

import { UserRole, UserStatus } from 'domain/enum/user';

import {
  requiredEmailField,
  requiredEnumField,
  requiredStringField,
} from 'utils/validation';

import { UserUpdateForm } from './types';

import type { SchemaOf } from 'yup';

export const schema: SchemaOf<UserUpdateForm> = object().shape({
  email: requiredEmailField(),
  lastName: requiredStringField(),
  firstName: requiredStringField(),
  status: requiredEnumField(UserStatus),
  role: requiredEnumField(UserRole),
});
