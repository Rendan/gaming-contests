import { FC, useMemo } from 'react';

import { useSignInForm } from 'hooks/forms/useSignInForm';
import { useProfile } from 'hooks/api/profile';

import { SignInForm } from '@organisms';

import { Entry } from '@templates';

import { getHandlers } from './handlers';

export const SignIn: FC = () => {
  const { loadProfile } = useProfile();

  const { handleAuthorize } = useMemo(
    () => getHandlers({ onAuthorizeSuccess: loadProfile }),
    [loadProfile],
  );

  const signInForm = useSignInForm({ onSubmit: handleAuthorize });

  return (
    <Entry>
      <div className="max-w-410 w-full">
        <SignInForm
          isDisabled={signInForm.isSubmitting}
          values={signInForm.values}
          touched={signInForm.touched}
          errors={signInForm.errors}
          onSubmit={signInForm.handleSubmit}
          onInputValueChange={signInForm.handleChange}
        />
      </div>
    </Entry>
  );
};
