import { FormikHelpers } from 'formik';

import { httpService } from 'services/http';

import { SignInForm } from 'forms/auth/signin/types';
import { invalidEmailOrPaswordFormError } from 'forms/auth/signin/errors';

type HandlersParams = {
  onAuthorizeSuccess: () => Promise<void>;
};

export const getHandlers = (handlersParams: HandlersParams) => ({
  handleAuthorize: async (
    values: SignInForm,
    formikHelpers: FormikHelpers<SignInForm>,
  ) => {
    try {
      await httpService.authorize(values);
      await handlersParams.onAuthorizeSuccess();
    } catch (error) {
      formikHelpers.setErrors(invalidEmailOrPaswordFormError);
    }
  },
});
