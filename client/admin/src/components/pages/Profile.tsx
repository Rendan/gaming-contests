import { FC } from 'react';

import { useProfile } from 'hooks/api/profile';

import { User } from '@organisms';

import { Dashboard } from '@templates';

export const Profile: FC = () => {
  const { profile } = useProfile();

  return (
    <Dashboard title="My Profile">
      <div className="flex">
        <div className="w-1/3">{profile && <User.Card user={profile} />}</div>
      </div>
    </Dashboard>
  );
};
