import { FormikHelpers } from 'formik';
import { useCallback } from 'react';
import { useNavigate } from 'react-router-dom';

import { signUp } from 'repositories/api/v1/auth';

import { SignUpFormValues } from 'forms/auth/signup/types';

import { appRoutes } from 'routes/appRoutes';

import { getFormikErrorsFromHttpRequestError } from 'utils/formik';

export const useHandlers = () => {
  const navigate = useNavigate();

  const handleSubmit = useCallback(
    async (
      values: SignUpFormValues,
      formikHelpers: FormikHelpers<SignUpFormValues>,
    ) => {
      try {
        await signUp(values);

        navigate(appRoutes.signIn());
      } catch (error) {
        const formikErrors = getFormikErrorsFromHttpRequestError(
          error as HttpRequestError<SignUpFormValues>,
        );
        formikHelpers.setErrors(formikErrors);
      }
    },
    [navigate],
  );

  return { handleSubmit };
};
