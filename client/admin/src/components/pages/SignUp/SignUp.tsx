import { FC } from 'react';

import { useSignUpForm } from 'hooks/forms/useSignUpForm';

import { SignUpForm } from '@organisms';

import { Entry } from '@templates';

import { useHandlers } from './hooks';

export const SignUp: FC = () => {
  const { handleSubmit } = useHandlers();

  const signUpForm = useSignUpForm({ onSubmit: handleSubmit });

  return (
    <Entry>
      <div className="max-w-410 w-full">
        <SignUpForm
          values={signUpForm.values}
          touched={signUpForm.touched}
          errors={signUpForm.errors}
          isDisabled={false}
          onSubmit={signUpForm.handleSubmit}
          onInputValueChange={signUpForm.handleChange}
        />
      </div>
    </Entry>
  );
};
