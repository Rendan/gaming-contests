import { useParams } from 'react-router-dom';
import { MouseEventHandler, useCallback } from 'react';

import { useUser, useUsersDeleting } from 'hooks/api/users';
import { useNavigateBack } from 'hooks/router/useNavigateBack';
import { useNavigateTo } from 'hooks/router/useNavigateTo';

import { appRoutes } from 'routes/appRoutes';

import { isRequestPending } from 'utils/http';

import { Confirmation } from '@molecules';

export const Delete = () => {
  const { id } = useParams();

  const { user } = useUser();
  const navigateBack = useNavigateBack();
  const navigateToUsers = useNavigateTo(appRoutes.users());

  const { requestState, deleteUser } = useUsersDeleting();

  const handleDeleteUser = useCallback<MouseEventHandler>(async () => {
    if (id) {
      await deleteUser(Number(id));

      navigateToUsers();
    }
  }, [id, deleteUser, navigateToUsers]);

  if (!user) {
    return null;
  }

  return (
    <div data-cy="delete-user-confirmation">
      <Confirmation
        title="Delete User"
        onClose={navigateBack}
        onConfirm={handleDeleteUser}
        confirmButtonColor="danger"
        confirmButtonText="Delete"
        isDisabled={isRequestPending(requestState)}
      >
        Are you sure you want to delete this user?
      </Confirmation>
    </div>
  );
};
