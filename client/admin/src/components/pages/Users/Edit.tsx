import { useNavigate, useParams } from 'react-router-dom';
import { useCallback } from 'react';

import { UserUpdateForm as UserUpdateFormValues } from 'forms/user/update/types';

import { useUser, useUsersUpdating } from 'hooks/api/users';

import { isRequestPending } from 'utils/http';

import { Modal } from '@molecules';

import { User as UserOrganisms } from '@organisms';

export const Edit = () => {
  const { id } = useParams();

  const { user } = useUser();
  const navigate = useNavigate();

  const { requestState, updateUser } = useUsersUpdating();

  const handleCloseModal = useCallback(() => navigate(-1), [navigate]);

  const handleUpdateUser = useCallback(
    async (values: UserUpdateFormValues) => {
      if (id) {
        await updateUser({ id: Number(id), payload: values });

        handleCloseModal();
      }
    },
    [id, updateUser, handleCloseModal],
  );

  if (!user) {
    return null;
  }

  return (
    <Modal title="Update User" onClose={handleCloseModal}>
      <UserOrganisms.UpdateForm
        user={user}
        onSubmit={handleUpdateUser}
        isDisabled={isRequestPending(requestState)}
      />
    </Modal>
  );
};
