import { FC, useEffect } from 'react';
import { Outlet, useParams } from 'react-router-dom';

import { useUser, useUsersUpdating } from 'hooks/api/users';
import { useNavigateTo } from 'hooks/router/useNavigateTo';

import { appRoutes } from 'routes/appRoutes';

import { IconButton } from '@atoms';

import { User as UserOrganisms } from '@organisms';

import { Dashboard } from '@templates';

export const Details: FC = () => {
  const { id } = useParams();
  const { user, loadUser, setData, resetData: resetUser } = useUser();

  const { user: updatedUser, resetData: resetUpdatedUser } = useUsersUpdating();

  const navigateToEditUserPage = useNavigateTo(appRoutes.userEdit(id));
  const navigateToDeleteUserPage = useNavigateTo(appRoutes.userDelete(id));

  useEffect(() => {
    if (id) {
      loadUser(Number(id)).catch(() => {});
    }
  }, [id]);

  useEffect(() => {
    return () => {
      resetUser();
      resetUpdatedUser();
    };
  }, []);

  useEffect(() => {
    setData(updatedUser);
  }, [updatedUser]);

  return (
    <>
      <Dashboard title="User Profile">
        <div className="flex">
          <div className="w-1/3">
            {user && (
              <div className="relative">
                <UserOrganisms.Card user={user} />
                <div className="absolute top-5 right-16 text-blue-800 dark:text-white">
                  <IconButton
                    iconName="pencil"
                    onClick={navigateToEditUserPage}
                  />
                </div>
                <div className="absolute top-5 right-5 text-blue-800 dark:text-white">
                  <IconButton
                    iconName="trash"
                    onClick={navigateToDeleteUserPage}
                    className="p-2"
                  />
                </div>
              </div>
            )}
          </div>
        </div>
      </Dashboard>
      <Outlet />
    </>
  );
};
