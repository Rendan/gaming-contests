import { List } from './List';
import { Details } from './Details';
import { Edit } from './Edit';
import { Delete } from './Delete';

export const Users = {
  List,
  Details,
  Edit,
  Delete,
};
