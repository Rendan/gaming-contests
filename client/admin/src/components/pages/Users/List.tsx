import { FC, useEffect, useMemo } from 'react';
import { useDebounce } from 'use-debounce';
import { Outlet } from 'react-router-dom';

import { UserRaw } from 'types/responses/api/v1/user';

import { useUsers } from 'hooks/api/users';
import { useUsersSearchForm } from 'hooks/forms/useUsersSearchForm';
import { useSorting } from 'hooks/ui/useSorting';

import { noop } from 'utils';

import { isRequestPending } from 'utils/http';
import { omitNullValuesKeys } from 'utils/object';

import { InfiniteScroll } from '@atoms';

import { User } from '@organisms';

import { Dashboard } from '@templates';

const usersLoadingMeta: MetaPayload = { limit: 30 };

export const List: FC = () => {
  const { values, handleChange } = useUsersSearchForm();
  const { sortBy, sortDirection, changeSorting } = useSorting<UserRaw>('id');
  const [submittedSearchForm] = useDebounce(values, 1000);
  const { users, requestState, loadMore, loadUsers, resetData } = useUsers();

  const usersLoadingParams = useMemo<PaginatedDataLoadingParams<UserRaw>>(
    () => ({
      sortBy,
      sortDirection,
      ...usersLoadingMeta,
      ...omitNullValuesKeys(submittedSearchForm),
    }),
    [sortBy, sortDirection, submittedSearchForm],
  );

  useEffect(() => {
    loadUsers(usersLoadingParams).catch(noop);
  }, [usersLoadingParams]);

  useEffect(() => {
    resetData();
  }, [sortBy, sortDirection]);

  useEffect(() => resetData, []);

  return (
    <>
      <Dashboard title="Users">
        <InfiniteScroll
          onLoadMore={() => loadMore(usersLoadingParams)}
          data={users}
          requestState={requestState}
        >
          <User.Table
            users={users}
            onSearchFormChange={handleChange}
            searchFormValues={values}
            sortBy={sortBy}
            sortDirection={sortDirection}
            isLoading={isRequestPending(requestState)}
            onSortingChange={changeSorting}
          />
        </InfiniteScroll>
      </Dashboard>
      <Outlet />
    </>
  );
};
