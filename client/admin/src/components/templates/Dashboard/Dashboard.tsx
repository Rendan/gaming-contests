import { ReactElement } from 'react';

import { useProfile } from 'hooks/api/profile';

import { Title } from '@atoms';

import { Navigation } from '@molecules';

import { Profile } from '@organisms';

type DashboardProps = {
  title: string;
  children: ReactElement | ReactElement[];
};

export const Dashboard = (props: DashboardProps) => {
  const { children, title } = props;
  const { profile } = useProfile();

  return (
    <div className="flex w-full min-h-screen">
      <div className="w-1/4 max-w-xs min-w-min dark:bg-blue-1000 transition">
        <div className="px-14 py-12 border-b border-gray-200 dark:border-gray-700 transition">
          <div className="font-poppins font-bold text-blue-800 dark:text-white text-2xl uppercase transition">
            Sovereign
          </div>
          <div className="font-poppins text-blue-800 dark:text-white text-2xl uppercase transition">
            Dashboard
          </div>
        </div>
        <div className="pl-8 pt-12">
          <Navigation />
        </div>
      </div>
      <div className="w-3/4 flex-1 bg-gray-200 dark:bg-blue-1100 transition">
        <div className="py-12 px-5">
          <div className="flex justify-between items-center mb-5">
            <div className="ml-2 mb-4">
              <Title>{title}</Title>
            </div>
            <div>
              <Profile.Toolbar profile={profile} />
            </div>
          </div>
          {children}
        </div>
      </div>
    </div>
  );
};
