import { FC, ReactElement } from 'react';

import { useTheme } from 'hooks/ui/useTheme';

import { IconButton } from '@atoms';

import classes from './styles.module.css';

interface EntryProps {
  children: ReactElement;
}

export const Entry: FC<EntryProps> = ({ children }) => {
  const { toggleTheme } = useTheme();

  return (
    <div className="flex m-auto items-center h-screen dark:bg-blue-1100 transition">
      <div className="basis-1/2 flex justify-center ">{children}</div>
      <div className="bg-white basis-1/2 h-full rounded-bl-[250px]">
        <div
          className={`rounded-bl-[250px] pr-5 pb-5 w-full flex justify-end items-end ${classes.background}`}
        >
          <IconButton
            data-cy="theme-switch-button"
            iconName="moon"
            onClick={toggleTheme}
            className=" border-gray-400 border w-16 h-16 bg-gradient-to-r from-purple-100 to-purple-200 dark:bg-gradient-to-r dark:from-purple-200 dark:to-blue-800 text-white dark:text-white transition hover:opacity-80"
          />
        </div>
      </div>
    </div>
  );
};
