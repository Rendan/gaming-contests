import { FC, SelectHTMLAttributes } from 'react';

import { Icon } from './Icon';

type SelectOption = {
  label: string;
  value: string | number;
};

type SelectProps = {
  options: Array<SelectOption>;
  isError?: boolean;
  isFullWidth?: boolean;
  size?: 'md' | 'xs';
  HtmlSelectProps: Partial<SelectHTMLAttributes<unknown>>;
};

export const Select: FC<SelectProps> = (props) => {
  const { size, isError, isFullWidth, options, HtmlSelectProps } = props;

  const widthClassName = isFullWidth ? 'w-full' : 'w-auto';
  const defaultColorClassName =
    'dark:bg-blue-1100 dark:border-gray-700 dark:text-white text-gray-600 border-gray-500 focus:border-blue-500 focus:border-blue-400 hover:border-blue-200 outline-blue-200';
  const errorColorClassName =
    'text-red-400 border-red-400 focus:border-red-800 hover:border-red-500 outline-red-500';
  const colorClassName = isError ? errorColorClassName : defaultColorClassName;
  const sizeClassName = size === 'md' ? 'pl-5 pr-7 py-3' : 'pl-4 pr-6 py-1';
  const iconPositionClassName = size === 'md' ? 'mt-[10px]' : 'mt-[2px]';

  return (
    <div className="relative text-gray-600 pointer-events-auto inline-block">
      <select
        data-cy={HtmlSelectProps.name}
        className={`${widthClassName} ${colorClassName} ${sizeClassName} appearance-none border text-gray-700 rounded-2xl transition disabled:opacity-50`}
        {...HtmlSelectProps}
      >
        {options.map((option) => (
          <option key={option.value} value={option.value}>
            {option.label}
          </option>
        ))}
      </select>
      <Icon
        className={`${iconPositionClassName} absolute right-3 pointer-events-none cursor-default top-3`}
        name="shevron"
      />
    </div>
  );
};

Select.defaultProps = {
  size: 'md',
  isFullWidth: false,
  isError: false,
};
