import { FC } from 'react';

interface TitleProps {
  children: string;
  variant?: 'xs' | 'sm';
}

export const Title: FC<TitleProps> = ({ children, variant = 'sm' }) => {
  const textClassName = variant === 'xs' ? 'text-2xl' : 'text-4xl';

  return (
    <div
      className={`${textClassName} font-bold text-blue-800 dark:text-white transition`}
    >
      {children}
    </div>
  );
};

Title.defaultProps = {
  variant: 'sm',
};
