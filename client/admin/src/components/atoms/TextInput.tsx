import { FC, InputHTMLAttributes } from 'react';

export interface TextInputProps {
  isFullWidth?: boolean;
  isError?: boolean;
  size?: 'md' | 'xs';
  HtmlInputProps: Partial<InputHTMLAttributes<unknown>>;
}

export const TextInput: FC<TextInputProps> = (props) => {
  const { isFullWidth, size = 'md', isError = false, HtmlInputProps } = props;
  const widthClassName = isFullWidth ? 'w-full' : 'w-auto';
  const defaultColorClassName =
    'dark:border-gray-700 dark:text-white placeholder-gray-600 border-gray-500 focus:border-blue-500 focus:border-blue-400 hover:border-blue-200 outline-blue-200';
  const errorColorClassName =
    'placeholder-red-400 border-red-400 focus:border-red-800 hover:border-red-500 outline-red-500 ';
  const colorClassName = isError ? errorColorClassName : defaultColorClassName;
  const sizeClassName = size === 'md' ? 'px-5 py-3' : 'px-4 py-1';

  return (
    <input
      data-cy={HtmlInputProps.name}
      className={`${widthClassName} ${colorClassName} ${sizeClassName} dark:bg-blue-1100 border text-gray-700 rounded-2xl transition disabled:opacity-50`}
      aria-invalid={isError ? 'true' : 'false'}
      {...HtmlInputProps}
    />
  );
};

TextInput.defaultProps = {
  isFullWidth: false,
  isError: false,
  size: 'md',
};
