import { FC, LabelHTMLAttributes } from 'react';

interface LabelProps extends LabelHTMLAttributes<unknown> {
  children: string;
  htmlFor: string | undefined;
  hasAsterisk?: boolean;
  isError?: boolean;
}

export const Label: FC<LabelProps> = (props) => {
  const { children, hasAsterisk, isError, htmlFor, ...rest } = props;
  const labelColorClassName = isError
    ? 'text-red-600'
    : 'text-blue-800 dark:text-white';
  const asteriskColorClassName = isError
    ? 'text-red-700'
    : 'text-blue-500 dark:text-purple-100';

  return (
    <label className="font-medium " htmlFor={htmlFor} {...rest}>
      <span className={`${labelColorClassName} transition`}>{children}</span>
      {hasAsterisk && (
        <span className={`${asteriskColorClassName} ml-0.5 transition`}>*</span>
      )}
    </label>
  );
};

Label.defaultProps = {
  hasAsterisk: false,
  isError: false,
};
