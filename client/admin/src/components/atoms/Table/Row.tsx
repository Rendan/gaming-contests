import { DropdownActions } from '@molecules/DropdownActions';

import { ReactElement } from 'react';

import { TableColumn, TableColumns } from 'types/components/table';
import { DropdownAction } from 'types/components/dropdownActions';

import { Link } from '@atoms';

type RowProps<T> = {
  columns: TableColumns<T>;
  data: T;
  dropdownActions?: Array<DropdownAction<T>>;
  getLink?: (data: T) => string;
};

const getFormattedValue = <T,>(tableColumn: TableColumn<T>, data: T) => {
  if (tableColumn.formatValue) {
    return tableColumn.formatValue(data[tableColumn.name]);
  }

  return String(data[tableColumn.name]);
};

export const Row = <T,>(props: RowProps<T>): ReactElement => {
  const { columns, data, dropdownActions, getLink } = props;

  return (
    <tr
      className="dark:even:bg-blue-1100 even:bg-gray-200 transition"
      data-cy="row"
    >
      {columns.map((column) => {
        const columnKey = column.name;

        const formattedValue = getFormattedValue(column, data);

        return (
          <td
            data-cy="cell"
            data-field={columnKey}
            key={String(columnKey)}
            className="px-5 py-3 first:pl-8 last:pr-8 dark:text-white transition"
          >
            {getLink ? (
              <Link
                className="text-blue-800 dark:text-white transition hover:underline"
                to={getLink(data)}
              >
                {String(formattedValue)}
              </Link>
            ) : (
              formattedValue
            )}
          </td>
        );
      })}
      {dropdownActions && (
        <td className="pl-5 py-3 pr-8 transition text-right">
          <DropdownActions target={data} actions={dropdownActions} />
        </td>
      )}
    </tr>
  );
};

Row.defaultProps = {
  getLink: null,
  dropdownActions: null,
};
