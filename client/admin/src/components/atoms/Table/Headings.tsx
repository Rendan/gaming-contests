import { ReactElement } from 'react';

import { TableHeading } from 'types/components/table';
import { SortDirection } from 'types/enums/sorting';

import { Icon } from '@atoms';

type HeadingsProps<T> = {
  headings: Array<TableHeading<T>>;
  sortBy: keyof T;
  sortDirection: SortDirection;
  isDisabled: boolean;
  hasDropdownActions: boolean;
  onSortingChange: React.MouseEventHandler<HTMLButtonElement>;
};

export const Headings = <T,>(props: HeadingsProps<T>): ReactElement => {
  const {
    headings,
    sortBy,
    sortDirection,
    isDisabled,
    hasDropdownActions,
    onSortingChange,
  } = props;

  const iconRotationClassName =
    sortDirection === SortDirection.ASC ? '' : ' rotate-180';

  return (
    <thead className="border-b border-gray-300 dark:border-gray-700 transition">
      <tr>
        {headings.map(({ name, label, width = 'auto' }) => (
          <th
            key={String(name)}
            style={{ width }}
            className="text-left pl-5 pr-8 py-3 first:pl-8 last:pr-8 text-gray-600 font-normal transition "
          >
            <button
              data-cy="change-sorting-button"
              data-name={name}
              data-selected={sortBy === name}
              data-sort-direction={sortDirection}
              onClick={onSortingChange}
              disabled={isDisabled}
              name={String(name)}
              type="button"
              className="flex items-center appearance-none disabled:opacity-50"
            >
              <span>{label}</span>
              {sortBy === name && (
                <Icon
                  name="shevron"
                  className={`ml-3${iconRotationClassName}`}
                />
              )}
            </button>
          </th>
        ))}
        {hasDropdownActions && <th> </th>}
      </tr>
    </thead>
  );
};
