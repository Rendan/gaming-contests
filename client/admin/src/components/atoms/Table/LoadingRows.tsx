import { times } from 'ramda';
import { FC } from 'react';

import { getRandomNumber } from 'utils/number';

type LoadingRowsProps = {
  columnsCount: number;
};

const DEFAULT_ROWS_COUNT = 30;

export const LoadingRows: FC<LoadingRowsProps> = (props: LoadingRowsProps) => {
  const { columnsCount } = props;

  return (
    <>
      <tr className="hidden">
        <td>
          <div className="w-1/6 w-2/6 w-3/6 w-4/6 w-5/6" />
        </td>
      </tr>
      {times(
        (rowIndex) => (
          <tr key={rowIndex}>
            {times(
              (columnIndex) => (
                <td
                  key={columnIndex}
                  className="px-5 py-3 first:pl-8 last:pr-8"
                >
                  <div className="animate-pulse flex">
                    <div
                      className={`rounded-full bg-slate-200 h-2 w-${getRandomNumber(
                        { min: 1, max: 5 },
                      )}/6`}
                    />
                  </div>
                </td>
              ),
              columnsCount,
            )}
          </tr>
        ),
        DEFAULT_ROWS_COUNT,
      )}
    </>
  );
};
