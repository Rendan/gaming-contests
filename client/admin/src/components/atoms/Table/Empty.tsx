import { FC } from 'react';

type EmptyProps = {
  columnsCount: number;
};

export const Empty: FC<EmptyProps> = (props: EmptyProps) => {
  const { columnsCount } = props;

  return (
    <tr data-cy="row">
      <td
        className="px-5 py-5 dark:text-white transition"
        colSpan={columnsCount}
      >
        There are no records
      </td>
    </tr>
  );
};
