import { FC, ReactElement } from 'react';
import { NavLink, NavLinkProps } from 'react-router-dom';

interface LinkProps extends NavLinkProps {
  to: string;
  children: string | ReactElement;
  className?: NavLinkProps['className'];
}

export const Link: FC<LinkProps> = (props) => {
  const { children, className, to } = props;
  return (
    <NavLink
      data-cy="link"
      className={
        className ||
        'text-primary hover:text-blue-900 transition font-medium dark:text-white'
      }
      to={to}
    >
      {children}
    </NavLink>
  );
};

Link.defaultProps = {
  className: undefined,
};
