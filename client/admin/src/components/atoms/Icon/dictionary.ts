/// <reference types="vite-plugin-svgr/client" />

import { ReactComponent as UserIcon } from 'assets/svg/user.svg';
import { ReactComponent as MoonIcon } from 'assets/svg/moon.svg';
import { ReactComponent as ShevronIcon } from 'assets/svg/shevron.svg';
import { ReactComponent as PlusIcon } from 'assets/svg/plus.svg';
import { ReactComponent as PencilIcon } from 'assets/svg/pencil.svg';
import { ReactComponent as DotsIcon } from 'assets/svg/dots.svg';
import { ReactComponent as TrashIcon } from 'assets/svg/trash.svg';

export const dictionary = {
  user: UserIcon,
  moon: MoonIcon,
  shevron: ShevronIcon,
  plus: PlusIcon,
  pencil: PencilIcon,
  dots: DotsIcon,
  trash: TrashIcon,
};
