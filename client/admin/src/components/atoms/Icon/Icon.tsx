import { FC } from 'react';

import { dictionary } from './dictionary';

type IconProps = {
  name: keyof typeof dictionary;
  className?: string;
};

export const Icon: FC<IconProps> = (props) => {
  const { name, className } = props;
  const TargetIcon = dictionary[name];

  return <TargetIcon className={className} />;
};

Icon.defaultProps = {
  className: undefined,
};
