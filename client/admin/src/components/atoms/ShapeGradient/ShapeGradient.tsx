export const ShapeGradient = () => {
  return (
    <div className="bg-gradient-to-r w-full h-full from-purple-100 to-purple-200 dark:bg-gradient-to-r dark:from-purple-200 dark:to-blue-800 transition" />
  );
};
