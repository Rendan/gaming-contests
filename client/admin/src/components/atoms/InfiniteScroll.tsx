import { isNil } from 'ramda';
import { ReactElement } from 'react';
import DefaultInfiniteScroll from 'react-infinite-scroll-component';

import { HttpRequestState } from 'types/enums/http';

import { isRequestPending } from 'utils/http';

type InfiniteScrollProps<T> = {
  children: ReactElement;
  data: Nullable<PaginatedData<T>>;
  requestState: HttpRequestState;
  onLoadMore: () => Promise<void>;
};

const hasMoreItems = <T,>(data: Nullable<PaginatedData<T>>) => {
  if (isNil(data)) {
    return false;
  }

  return data.meta.currentPage < data.meta.totalPages;
};

export const InfiniteScroll = <T,>(
  props: InfiniteScrollProps<T>,
): ReactElement => {
  const { children, data, requestState, onLoadMore } = props;
  const hasMore = !isRequestPending(requestState) && hasMoreItems(data);
  const dataLength = data?.items.length || 0;

  return (
    <DefaultInfiniteScroll
      next={onLoadMore}
      hasMore={hasMore}
      loader={null}
      dataLength={dataLength}
    >
      {children}
    </DefaultInfiniteScroll>
  );
};
