import { DOMAttributes, FC } from 'react';

import { Icon } from '@atoms';

import { dictionary } from './Icon/dictionary';

type IconButtonProps = {
  iconName: keyof typeof dictionary;
  className?: Nullable<string>;
  isDisabled?: boolean;
  'data-cy'?: string;
  onClick: DOMAttributes<HTMLButtonElement>['onClick'];
};

export const IconButton: FC<IconButtonProps> = (props) => {
  const { iconName, className, isDisabled, onClick } = props;

  return (
    <button
      type="button"
      onClick={onClick}
      disabled={isDisabled}
      data-cy={props['data-cy']}
      className={`w-8 h-8 rounded-full flex items-center justify-center disabled:opacity-50 ${
        className || ''
      }`}
    >
      <Icon name={iconName} />
    </button>
  );
};

IconButton.defaultProps = {
  className: null,
  isDisabled: false,
  'data-cy': undefined,
};
