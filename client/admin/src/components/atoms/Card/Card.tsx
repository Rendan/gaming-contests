import { FC, ReactElement } from 'react';

type CardProps = {
  title?: Nullable<string>;
  children: ReactElement;
};

export const Card: FC<CardProps> = (props) => {
  const { title, children } = props;

  return (
    <div className="bg-white rounded-3xl dark:bg-blue-1000 transition">
      {title && (
        <div className="p-8 font-semibold text-blue-800 dark:text-white text-2xl transition">
          {title}
        </div>
      )}
      <div>{children}</div>
    </div>
  );
};

Card.defaultProps = {
  title: null,
};
