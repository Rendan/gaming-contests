import { FC, ButtonHTMLAttributes, useMemo } from 'react';

type ButtonVariant = 'contained' | 'text' | 'outlined';
type ButtonColor = 'primary' | 'danger' | 'success' | 'neutral';
type ButtonSize = 'md' | 'sm' | 'xs';

interface ButtonProps {
  children: string;
  isFullWidth?: boolean;
  size?: ButtonSize;
  variant?: ButtonVariant;
  color?: ButtonColor;
  'data-cy'?: string;
  HtmlButtonProps: ButtonHTMLAttributes<unknown>;
}

const getContainedBackgroundClassName = (color: ButtonColor) => {
  switch (color) {
    case 'success':
      return 'border bg-green-600 border-green-600 text-white hover:bg-green-700 hover:border-green-700 focus-visible:bg-green-900 focus-visible:border-green-900 focus-visible:outline-green-400 focus-visible:outline disabled:border-green-400 disabled:bg-green-400';
    case 'danger':
      return 'border bg-red-600 border-red-600 text-white hover:bg-red-700 hover:border-red-700 focus-visible:bg-red-700 focus-visible:border-red-700 focus-visible:outline-red-400 focus-visible:outline disabled:border-red-400 disabled:bg-red-400';
    case 'primary':
      return 'border bg-blue-500 border-blue-500 text-white hover:bg-blue-900 hover:border-blue-900 focus-visible:bg-blue-900 focus-visible:border-blue-900 focus-visible:outline-blue-400 focus-visible:outline disabled:border-blue-400 disabled:bg-blue-400 dark:bg-purple-100 dark:border-purple-100 dark:hover:bg-purple-400 dark:focus:bg-purple-400 dark:hover:border-purple-400 focus-visible:outline-purple-200';
    default:
      return '';
  }
};

const getTextColorClassName = (color: ButtonColor) => {
  switch (color) {
    case 'neutral':
      return 'dark:text-white text-blue-800 hover:underline';
    case 'success':
      return 'text-green-600 hover:underline';
    case 'danger':
      return 'text-red-500 hover:underline';
    case 'primary':
      return 'text-blue-500 hover:underline';
    default:
      return '';
  }
};

const getOutlinedBackgroundClassName = (color: ButtonColor) => {
  switch (color) {
    case 'success':
      return 'border text-green-600';
    case 'danger':
      return 'border text-red-500';
    case 'primary':
      return 'border text-blue-500';
    default:
      return '';
  }
};

const getColorsClassName = (variant: ButtonVariant, color: ButtonColor) => {
  switch (variant) {
    case 'contained':
      return getContainedBackgroundClassName(color);
    case 'text':
      return getTextColorClassName(color);
    case 'outlined':
      return `${getTextColorClassName(color)} ${getOutlinedBackgroundClassName(
        color,
      )}`;
    default:
      return '';
  }
};

const getSizeClassName = (size: ButtonSize) => {
  switch (size) {
    case 'md':
      return 'py-4 px-8';
    case 'sm':
      return 'py-3 px-6';
    case 'xs':
      return 'py-1 px-4';
    default:
      return '';
  }
};

export const Button: FC<ButtonProps> = (props) => {
  const {
    children,
    color = 'primary',
    isFullWidth,
    size = 'sm',
    variant = 'contained',
    HtmlButtonProps,
  } = props;
  const widthClassName = isFullWidth ? 'w-full' : 'w-auto';
  const sizeClassName = useMemo(() => getSizeClassName(size), [size]);
  const colorsClassName = useMemo(
    () => getColorsClassName(variant, color),
    [variant, color],
  );

  return (
    <button
      data-cy={props['data-cy']}
      type={HtmlButtonProps.type || 'submit'}
      className={`${widthClassName} ${sizeClassName} ${colorsClassName} transition text-center text-sm rounded-2xl hover:disabled:cursor-not-allowed`}
      {...HtmlButtonProps}
    >
      {children}
    </button>
  );
};

Button.defaultProps = {
  isFullWidth: false,
  'data-cy': undefined,
  size: 'sm',
  color: 'primary',
  variant: 'contained',
};
