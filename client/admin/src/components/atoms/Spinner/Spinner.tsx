import classNames from './Spinner.module.css';

export const Spinner = () => {
  return (
    <div className={classNames.spinner}>
      <div />
      <div />
      <div />
    </div>
  );
};
