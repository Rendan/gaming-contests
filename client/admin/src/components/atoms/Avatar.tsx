import { FC } from 'react';

type AvatarProps = {
  imageUrl: string | null;
  initials: string;
  size?: 'sm' | 'xs';
};

export const Avatar: FC<AvatarProps> = (props) => {
  const { imageUrl, size, initials } = props;

  const sizeClassName = size === 'xs' ? 'w-10 h-10' : 'w-24 h-24';
  const fontSizeClassName = size === 'xs' ? 'text-sm' : 'text-3xl';

  if (typeof imageUrl === 'string') {
    return (
      <img
        src={imageUrl}
        className={`rounded-full ${sizeClassName}`}
        alt="User"
      />
    );
  }

  return (
    <div
      className={`rounded-full bg-blue-800 ${sizeClassName} ${fontSizeClassName} flex items-center justify-center text-white`}
    >
      {initials}
    </div>
  );
};

Avatar.defaultProps = {
  size: 'xs',
};
