import { FC, useMemo } from 'react';
import { useFormik } from 'formik';
import { NavigateFunction, useNavigate } from 'react-router-dom';

import { User } from 'domain/models';

import { SortDirection } from 'types/enums/sorting';
import { UserRaw } from 'types/responses/api/v1/user';
import { DropdownAction } from 'types/components/dropdownActions';

import { UsersSearchFormValues } from 'hooks/forms/useUsersSearchForm';

import { appRoutes } from 'routes/appRoutes';

import { Table as DefaultTable } from '@organisms';

import {
  USERS_TABLE_COLUMNS,
  USERS_TABLE_HEADINGS,
  USERS_TABLE_SEARCH_FIELDS,
} from './constants';

type TableProps = {
  users: Nullable<PaginatedData<User>>;
  searchFormValues: UsersSearchFormValues;
  isLoading: boolean;
  sortBy: keyof UserRaw;
  sortDirection: SortDirection;
  onSearchFormChange: ReturnType<typeof useFormik>['handleChange'];
  onSortingChange: React.MouseEventHandler<HTMLButtonElement>;
};

const getUserLink = (user: UserRaw) => appRoutes.user(user.id);

const getDropdownActions = (
  navigate: NavigateFunction,
): Array<DropdownAction<UserRaw>> => [
  {
    label: 'View',
    onClick: (user) => navigate(appRoutes.user(user.id)),
  },
  {
    label: 'Edit',
    onClick: (user) => navigate(appRoutes.userEdit(user.id)),
  },
  {
    label: 'Delete',
    onClick: (user) => navigate(appRoutes.userDelete(user.id)),
  },
];

export const Table: FC<TableProps> = (props) => {
  const {
    users,
    searchFormValues,
    isLoading,
    sortBy,
    sortDirection,
    onSortingChange,
    onSearchFormChange,
  } = props;

  const navigate = useNavigate();

  const dropdownActions = useMemo(
    () => getDropdownActions(navigate),
    [navigate],
  );

  return (
    <div data-cy="users-list">
      <form data-cy="users-list-search-form">
        <DefaultTable<UserRaw, UserRaw>
          headings={USERS_TABLE_HEADINGS}
          columns={USERS_TABLE_COLUMNS}
          getRowLink={getUserLink}
          searchFields={USERS_TABLE_SEARCH_FIELDS}
          onSearchFormChange={onSearchFormChange}
          onSortingChange={onSortingChange}
          searchFormValues={searchFormValues}
          data={users?.items}
          meta={users?.meta}
          isLoading={isLoading}
          sortBy={sortBy}
          sortDirection={sortDirection}
          dropdownActions={dropdownActions}
          title="Users"
          primaryField="id"
        />
      </form>
    </div>
  );
};
