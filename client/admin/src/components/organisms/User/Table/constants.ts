import { UserRoleLabel, UserStatusLabel } from 'domain/enum/user';

import { TableColumns, TableHeading } from 'types/components/table';
import { UserRaw } from 'types/responses/api/v1/user';

import {
  createSelectInputField,
  createSelectOptionsFromEnum,
  createTextInputField,
} from 'utils/forms';
import { createEnumLabelGetter } from 'utils/object';

export const USERS_TABLE_SEARCH_FIELDS = [
  createTextInputField<UserRaw>('id', 'Search by ID'),
  createTextInputField<UserRaw>('email', 'Search by Email'),
  createTextInputField<UserRaw>('firstName', 'Search by First Name'),
  createTextInputField<UserRaw>('lastName', 'Search by Last Name'),
  createSelectInputField<UserRaw>({
    name: 'status',
    placeholder: 'Select User status',
    options: createSelectOptionsFromEnum(UserStatusLabel),
  }),
  createSelectInputField<UserRaw>({
    name: 'role',
    placeholder: 'Select User role',
    options: createSelectOptionsFromEnum(UserRoleLabel),
  }),
];

export const USERS_TABLE_HEADINGS: Array<TableHeading<UserRaw>> = [
  { name: 'id', label: 'ID', width: '250px' },
  { name: 'email', label: 'Email' },
  { name: 'firstName', label: 'First Name', width: '250px' },
  { name: 'lastName', label: 'Last Name', width: '250px' },
  { name: 'status', label: 'Status', width: '150px' },
  { name: 'role', label: 'Role', width: '250px' },
];

export const USERS_TABLE_COLUMNS: TableColumns<UserRaw> = [
  { name: 'id' },
  { name: 'email' },
  { name: 'firstName' },
  { name: 'lastName' },
  {
    name: 'status' as keyof UserRaw,
    formatValue: createEnumLabelGetter(UserStatusLabel),
  },
  {
    name: 'role' as keyof UserRaw,
    formatValue: createEnumLabelGetter(UserRoleLabel),
  },
];
