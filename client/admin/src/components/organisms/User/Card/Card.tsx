import { FC } from 'react';

import { User } from 'domain/models';

import { Avatar, ShapeGradient, Card as DefaultCard } from '@atoms';

type CardProps = {
  user: User;
};

export const Card: FC<CardProps> = (props) => {
  const { user } = props;

  return (
    <DefaultCard>
      <div className="p-3 flex flex-col items-center">
        <div className="rounded-3xl overflow-hidden h-36 w-full">
          <ShapeGradient />
        </div>
        <div className="rounded-full p-1 dark:bg-blue-1000 bg-white -mt-14 transition mb-3">
          <Avatar
            imageUrl={user.avatarUrl}
            initials={user.initials}
            size="sm"
          />
        </div>
        <div className="text-blue-800 dark:text-white text-xl transition">
          {user.fullName}
        </div>
        <div className="mb-5">
          <div className="text-gray-400 text-sm transition">
            {user.roleFormatted}
          </div>
        </div>
      </div>
    </DefaultCard>
  );
};
