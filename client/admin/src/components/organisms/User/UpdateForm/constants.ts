import { UserRoleLabel, UserStatusLabel } from 'domain/enum/user';

import { TextInputField } from 'types/components/input';
import { SelectField } from 'types/components/select';
import { FormField } from 'types/components/form';
import { FormFieldTypeEnum } from 'types/enums/ui';

import { UserUpdateForm as UserUpdateFormValues } from 'forms/user/update/types';

import { createSelectOptionsFromEnum } from 'utils/forms';

export type UserUpdateFormField<T> = (TextInputField<T> | SelectField<T>) & {
  label: string;
  id: keyof T;
};

export const fields: FormField<UserUpdateFormValues>[] = [
  {
    label: 'First Name',
    name: 'firstName',
    id: 'firstName',
    placeholder: 'Enter First Name',
    type: FormFieldTypeEnum.Text,
  },
  {
    label: 'Last Name',
    name: 'lastName',
    id: 'lastName',
    placeholder: 'Enter Last Name',
    type: FormFieldTypeEnum.Text,
  },
  {
    label: 'Email',
    name: 'email',
    id: 'email',
    placeholder: 'mail@simmmple.com',
    type: FormFieldTypeEnum.Text,
  },
  {
    label: 'Role',
    name: 'role',
    id: 'role',
    placeholder: 'Choose Role',
    type: 'select',
    options: createSelectOptionsFromEnum(UserRoleLabel, false),
  },
  {
    label: 'Status',
    name: 'status',
    id: 'status',
    placeholder: 'Choose Status',
    type: 'select',
    options: createSelectOptionsFromEnum(UserStatusLabel, false),
  },
];
