import { useFormik } from 'formik';
import { FC } from 'react';

import { User } from 'domain/models';

import { UserUpdateForm as UserUpdateFormValues } from 'forms/user/update/types';
import { schema } from 'forms/user/update/schema';

import { Button } from '@atoms';

import { FormField } from '@molecules';

import { fields } from './constants';

interface UpdateFormProps {
  user: User;
  isDisabled: boolean;

  onSubmit: (formValues: UserUpdateFormValues) => Promise<void>;
}

export const UpdateForm: FC<UpdateFormProps> = (props) => {
  const { user, isDisabled, onSubmit } = props;
  const { values, errors, touched, handleSubmit, handleChange } =
    useFormik<UserUpdateFormValues>({
      initialValues: user.user,
      validationSchema: schema,
      onSubmit,
    });

  return (
    <form onSubmit={handleSubmit} data-cy="user-edit-form">
      {fields.map((field) => {
        const { label, ...rest } = field;

        return (
          <FormField<UserUpdateFormValues>
            key={field.name}
            field={field}
            isFullWidth
            isRequired
            value={values[field.name]}
            isError={!!errors[field.name] && touched[field.name]}
            helperText={errors[field.name]}
            isDisabled={isDisabled}
            HtmlInputProps={rest}
            onChange={handleChange}
          />
        );
      })}
      <div className="mt-8">
        <Button
          data-cy="submit-button"
          isFullWidth
          size="md"
          HtmlButtonProps={{ disabled: isDisabled }}
        >
          Save
        </Button>
      </div>
    </form>
  );
};
