import { useFormik } from 'formik';

import { TableColumns, TableHeading } from 'types/components/table';
import { TextInputField } from 'types/components/input';
import { SelectField } from 'types/components/select';
import { SortDirection } from 'types/enums/sorting';
import { DropdownAction } from 'types/components/dropdownActions';

import { SearchFormValues } from 'hooks/forms/useUsersSearchForm';

import { Table as TableAtoms, Card } from '@atoms';

import { Table as TableMolecules } from '@molecules';

type TableProps<T, P> = {
  title: string;
  headings: Array<TableHeading<P>>;
  primaryField: keyof T;
  sortBy: keyof P;
  sortDirection: SortDirection;
  columns: TableColumns<T>;
  searchFormValues?: SearchFormValues<T>;
  data?: Array<T>;
  meta?: Meta;
  searchFields?: Array<SelectField<T> | TextInputField<T>>;
  dropdownActions?: Array<DropdownAction<T>>;
  isLoading: boolean;
  getRowLink?: (data: T) => string;
  onSearchFormChange?: ReturnType<typeof useFormik>['handleChange'];
  onSortingChange: React.MouseEventHandler<HTMLButtonElement>;
};

export type TableSearchField<T> = {
  type: 'text' | 'select';
  name: keyof T;
  options?: Array<string>;
  placeholder: string;
};

export const Table = <T, P>(props: TableProps<T, P>) => {
  const {
    title,
    headings,
    data,
    meta,
    primaryField,
    searchFields,
    columns,
    searchFormValues,
    isLoading,
    sortBy,
    sortDirection,
    dropdownActions,
    getRowLink,
    onSearchFormChange,
    onSortingChange,
  } = props;

  const shouldRenderSearchFields =
    searchFields && searchFormValues && !!onSearchFormChange;
  const shouldRenderEmptyDataMessage =
    meta?.currentPage === 1 && meta.totalItems === 0 && !isLoading;

  return (
    <Card title={title}>
      <div className={dropdownActions ? 'pb-36' : 'pb-4'}>
        <table className="w-full">
          <TableAtoms.Headings<P>
            sortDirection={sortDirection}
            sortBy={sortBy}
            headings={headings}
            isDisabled={isLoading}
            onSortingChange={onSortingChange}
            hasDropdownActions={!!dropdownActions}
          />
          <tbody data-cy="table-body">
            {shouldRenderSearchFields && (
              <TableMolecules.SearchFields<T>
                searchFields={searchFields}
                searchFormValues={searchFormValues}
                onChange={onSearchFormChange}
              />
            )}
            {data &&
              data.map((row) => (
                <TableAtoms.Row<T>
                  key={String(row[primaryField])}
                  data={row}
                  columns={columns}
                  getLink={getRowLink}
                  dropdownActions={dropdownActions}
                />
              ))}
            {isLoading && (
              <TableAtoms.LoadingRows columnsCount={headings.length} />
            )}
            {shouldRenderEmptyDataMessage && (
              <TableAtoms.Empty columnsCount={headings.length} />
            )}
          </tbody>
        </table>
      </div>
    </Card>
  );
};

Table.defaultProps = {
  data: null,
  meta: null,
  searchFields: null,
  onSearchFormChange: undefined,
  searchFormValues: undefined,
  getRowLink: null,
  dropdownActions: null,
};
