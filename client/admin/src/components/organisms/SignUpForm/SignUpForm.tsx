import { FormikErrors, FormikTouched } from 'formik';
import { FC } from 'react';

import { SignUpFormValues } from 'forms/auth/signup/types';

import { appRoutes } from 'routes/appRoutes';

import { Button, Link, Title } from '@atoms';

import { FormField } from '@molecules';

import { fields } from './constants';

interface SignUpFormProps {
  values: SignUpFormValues;
  errors: FormikErrors<SignUpFormValues>;
  touched: FormikTouched<SignUpFormValues>;
  isDisabled: boolean;

  onSubmit: (event?: React.FormEvent<HTMLFormElement> | undefined) => void;
  onInputValueChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
}

export const SignUpForm: FC<SignUpFormProps> = (props) => {
  const { values, touched, errors, isDisabled, onSubmit, onInputValueChange } =
    props;

  return (
    <>
      <div className="mb-2">
        <Title>Sign Up</Title>
      </div>
      <p className="text-gray-600 mb-6">Enter your registration info</p>
      <form onSubmit={onSubmit}>
        {fields.map((field) => {
          return (
            <FormField<SignUpFormValues>
              key={field.name}
              field={field}
              isFullWidth
              isRequired
              value={values[field.name]}
              isError={!!errors[field.name] && touched[field.name]}
              helperText={errors[field.name]}
              isDisabled={isDisabled}
              onChange={onInputValueChange}
              HtmlInputProps={{
                id: field.name,
                name: field.name,
              }}
            />
          );
        })}
        <div className="mt-12">
          <Button
            HtmlButtonProps={{ disabled: isDisabled }}
            isFullWidth
            size="md"
          >
            Sign Up
          </Button>
        </div>
        <div className="mt-6">
          <span className="mr-2 dark:text-gray-400 transition">
            Already have an account?
          </span>
          <Link to={appRoutes.signIn()}>Sign In</Link>
        </div>
      </form>
    </>
  );
};
