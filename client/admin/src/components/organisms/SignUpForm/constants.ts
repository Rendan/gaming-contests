import { FormField } from 'types/components/form';
import { FormFieldTypeEnum } from 'types/enums/ui';

import { SignUpFormValues } from 'forms/auth/signup/types';

export const fields: Array<FormField<SignUpFormValues>> = [
  {
    label: 'First Name',
    name: 'firstName',
    id: 'firstName',
    placeholder: 'John',
    type: FormFieldTypeEnum.Text,
  },
  {
    label: 'Last Name',
    name: 'lastName',
    id: 'lastName',
    placeholder: 'Doe',
    type: FormFieldTypeEnum.Text,
  },
  {
    label: 'Email',
    name: 'email',
    id: 'email',
    placeholder: 'john.doe@gmail.com',
    type: FormFieldTypeEnum.Text,
  },
  {
    label: 'Password',
    name: 'password',
    id: 'password',
    type: FormFieldTypeEnum.Password,
    placeholder: 'Min. 8 characters',
  },
  {
    label: 'Password Confirmation',
    name: 'passwordConfirmation',
    id: 'passwordConfirmation',
    type: FormFieldTypeEnum.Password,
    placeholder: 'Confirm your password',
  },
];
