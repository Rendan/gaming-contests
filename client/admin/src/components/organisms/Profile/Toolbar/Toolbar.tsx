import { FC } from 'react';

import { Profile } from 'domain/models';

import { useTheme } from 'hooks/ui/useTheme';

import { appRoutes } from 'routes/appRoutes';

import { Avatar, IconButton, Link } from '@atoms';

type ToolbarProps = {
  profile: Nullable<Profile>;
};

export const Toolbar: FC<ToolbarProps> = (props) => {
  const { profile } = props;

  const { toggleTheme } = useTheme();

  return (
    <div className="bg-white dark:bg-blue-1000 flex justify-end p-3 rounded-full items-center transition">
      <div className="ml-3">
        <IconButton
          data-cy="theme-switch-button"
          iconName="moon"
          onClick={toggleTheme}
          className="text-gray-600 dark:text-white dark:hover:text-primary hover:text-blue-800 hover:bg-gray-300"
        />
      </div>
      {profile && (
        <div className="ml-3">
          <Link to={appRoutes.profile()}>
            <Avatar imageUrl={null} initials={profile.initials} />
          </Link>
        </div>
      )}
    </div>
  );
};
