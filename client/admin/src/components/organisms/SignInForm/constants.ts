import { FormField } from 'types/components/form';
import { FormFieldTypeEnum } from 'types/enums/ui';

import { SignInForm } from 'forms/auth/signin/types';

export const fields: Array<FormField<SignInForm>> = [
  {
    label: 'Email',
    name: 'email',
    id: 'email',
    placeholder: 'mail@simmmple.com',
    type: FormFieldTypeEnum.Text,
  },
  {
    label: 'Password',
    name: 'password',
    id: 'password',
    type: FormFieldTypeEnum.Password,
    placeholder: 'Min. 8 characters',
  },
];
