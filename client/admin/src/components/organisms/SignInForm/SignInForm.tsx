import { FormikErrors, FormikTouched } from 'formik';
import { ChangeEventHandler, FC } from 'react';

import { SignInForm as SignInFormValues } from 'forms/auth/signin/types';

import { appRoutes } from 'routes/appRoutes';

import { Button, Link, Title } from '@atoms';

import { FormField } from '@molecules';

import { fields } from './constants';

interface SignInFormProps {
  values: SignInFormValues;
  errors: FormikErrors<SignInFormValues>;
  touched: FormikTouched<SignInFormValues>;
  isDisabled: boolean;

  onSubmit: (event?: React.FormEvent<HTMLFormElement> | undefined) => void;
  onInputValueChange: ChangeEventHandler;
}

export const SignInForm: FC<SignInFormProps> = (props) => {
  const { values, touched, errors, isDisabled, onSubmit, onInputValueChange } =
    props;

  return (
    <>
      <div className="mb-2">
        <Title>Sign In</Title>
      </div>
      <p className="text-gray-600 mb-6">
        Enter your email and password to sign in!
      </p>
      <form onSubmit={onSubmit} data-cy="sign-in">
        {fields.map((field) => {
          return (
            <FormField<SignInFormValues>
              key={field.name}
              field={field}
              isFullWidth
              isRequired
              value={values[field.name]}
              isError={!!errors[field.name] && touched[field.name]}
              helperText={errors[field.name]}
              isDisabled={isDisabled}
              onChange={onInputValueChange}
              HtmlInputProps={{
                id: field.name,
                name: field.name,
              }}
            />
          );
        })}
        <div className="text-right mt-6 mb-6">
          <Link to={appRoutes.forgotPassword()}>Forgot password?</Link>
        </div>
        <Button
          isFullWidth
          size="md"
          HtmlButtonProps={{ disabled: isDisabled }}
          color="primary"
          variant="contained"
        >
          Sign In
        </Button>
        <div className="mt-6">
          <span className="mr-2 dark:text-gray-400 transition">
            Not registered yet?
          </span>
          <Link to={appRoutes.signUp()}>Create an Account</Link>
        </div>
      </form>
    </>
  );
};
