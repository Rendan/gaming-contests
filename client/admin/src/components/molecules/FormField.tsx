import { ChangeEventHandler, InputHTMLAttributes } from 'react';

import { FormField as FormFieldType } from 'types/components/form';
import { FormFieldTypeEnum } from 'types/enums/ui';

import { Label, Select, TextInput, TextInputProps } from '@atoms';

export interface FormFieldProps<T> extends TextInputProps {
  value: string;
  field: FormFieldType<T>;
  helperText?: string;
  isRequired?: boolean;
  isDisabled?: boolean;
  isFullWidth?: boolean;
  isError?: boolean;
  HtmlInputProps: InputHTMLAttributes<unknown>;

  onChange: ChangeEventHandler;
}

export const FormField = <T,>(props: FormFieldProps<T>) => {
  const {
    value,
    helperText,
    isRequired,
    isFullWidth,
    isDisabled,
    isError,
    field,
    onChange,
    HtmlInputProps,
  } = props;
  const { id, name } = HtmlInputProps;
  const helperTextID = name ? `${name}-hint` : undefined;

  return (
    <div className="mb-5">
      {field.label && (
        <Label
          className="mb-2 block"
          isError={isError}
          htmlFor={id}
          hasAsterisk={isRequired}
        >
          {field.label}
        </Label>
      )}
      {(field.type === FormFieldTypeEnum.Text ||
        field.type === FormFieldTypeEnum.Password) && (
        <TextInput
          isFullWidth={isFullWidth}
          isError={isError}
          HtmlInputProps={{
            id,
            name,
            value,
            onChange,
            type: field.type,
            disabled: isDisabled,
            'aria-describedby': helperTextID,
            ...HtmlInputProps,
          }}
        />
      )}
      {field.type === FormFieldTypeEnum.Select && (
        <Select
          isFullWidth={isFullWidth}
          options={field.options}
          HtmlSelectProps={{
            onChange,
            name: String(field.name),
            placeholder: field.placeholder,
            value,
          }}
        />
      )}

      {isError && helperText && (
        <p className="mt-1 ml-5 text-xs text-red-500">{helperText}</p>
      )}
    </div>
  );
};

FormField.defaultProps = {
  helperText: undefined,
  isFullWidth: false,
  isRequired: false,
  isDisabled: false,
  isError: false,
};
