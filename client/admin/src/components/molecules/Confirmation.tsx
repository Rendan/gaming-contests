import { FC, MouseEventHandler, ReactElement } from 'react';

import { Button } from '@atoms';

import { Modal } from './Modal';

type ConfirmationProps = {
  title: string;
  children: ReactElement | ReactElement[] | string;
  confirmButtonText?: string;
  confirmButtonColor?: 'danger' | 'primary';
  isDisabled: boolean;
  onClose: () => void;
  onConfirm: MouseEventHandler;
};

export const Confirmation: FC<ConfirmationProps> = (props) => {
  const {
    children,
    title,
    confirmButtonColor = 'primary',
    confirmButtonText = 'Confirm',
    isDisabled,
    onClose,
    onConfirm,
  } = props;

  return (
    <Modal title={title} onClose={onClose} isDisabled={isDisabled}>
      <div data-cy="confirmation-body">
        <div className="dark:text-white mb-9">{children}</div>
        <div className="flex justify-end gap-3">
          <Button
            data-cy="cancel"
            variant="outlined"
            color="neutral"
            HtmlButtonProps={{ onClick: onClose, disabled: isDisabled }}
          >
            Cancel
          </Button>
          <Button
            data-cy="confirm"
            variant="contained"
            color={confirmButtonColor}
            HtmlButtonProps={{ onClick: onConfirm, disabled: isDisabled }}
          >
            {confirmButtonText}
          </Button>
        </div>
      </div>
    </Modal>
  );
};

Confirmation.defaultProps = {
  confirmButtonText: 'Confirm',
  confirmButtonColor: 'primary',
};
