import { useFormik } from 'formik';

import { SelectField } from 'types/components/select';
import { TextInputField } from 'types/components/input';
import { FormFieldTypeEnum } from 'types/enums/ui';

import { SearchFormValues } from 'hooks/forms/useUsersSearchForm';

import { Select, TextInput } from '@atoms';

export type SearchFieldProps<T> = {
  searchFields: Array<TextInputField<T> | SelectField<T>>;
  searchFormValues: SearchFormValues<T>;
  onChange: ReturnType<typeof useFormik>['handleChange'];
};

export const SearchFields = <T,>(props: SearchFieldProps<T>) => {
  const { searchFields, searchFormValues, onChange } = props;

  return (
    <tr>
      {searchFields.map((searchField, index) => (
        <td key={String(searchField.name)} className="py-4">
          {searchField.type === FormFieldTypeEnum.Text && (
            <div className={index ? 'ml-1' : 'ml-4'}>
              <TextInput
                size="xs"
                HtmlInputProps={{
                  onChange,
                  name: String(searchField.name),
                  placeholder: searchField.placeholder,
                  value: searchFormValues[searchField.name],
                }}
              />
            </div>
          )}
          {searchField.type === FormFieldTypeEnum.Select && (
            <div className={index ? 'ml-1' : 'ml-4'}>
              <Select
                size="xs"
                options={searchField.options}
                HtmlSelectProps={{
                  onChange,
                  name: String(searchField.name),
                  placeholder: searchField.placeholder,
                  value: searchFormValues[searchField.name],
                }}
              />
            </div>
          )}
        </td>
      ))}
    </tr>
  );
};
