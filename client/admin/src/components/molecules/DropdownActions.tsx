import { useClickOutside } from 'usable-react';
import { useCallback, useMemo, useRef } from 'react';

import { DropdownAction } from 'types/components/dropdownActions';

import { useModal } from 'hooks/ui';

import { Button, IconButton } from '@atoms';

type DropdownActionsProps<T> = {
  target: T;
  actions: Array<DropdownAction<T>>;
  verticalAlignment?: 'top' | 'bottom';
  horizontalAlignment?: 'left' | 'right';
};

export const DropdownActions = <T,>(props: DropdownActionsProps<T>) => {
  const {
    actions,
    verticalAlignment = 'bottom',
    horizontalAlignment = 'left',
    target,
  } = props;
  const ref = useRef<HTMLDivElement>(null);
  const { isOpen, toggleModal, closeModal } = useModal();

  const handleClickAction = useCallback(
    (action: DropdownAction<T>) => () => {
      action.onClick(target);
    },
    [target],
  );

  const actionsListStyle = useMemo(() => {
    return {
      [verticalAlignment === 'top' ? 'bottom' : 'top']: '100%',
      [horizontalAlignment === 'left' ? 'right' : 'left']: '100%',
    };
  }, [verticalAlignment, horizontalAlignment]);

  useClickOutside(ref, closeModal);

  return (
    <div
      className="relative inline-block dark:text-white text-blue-800 transition"
      ref={ref}
    >
      <IconButton
        data-cy="dropdown-actions-toggle"
        iconName="dots"
        onClick={toggleModal}
      />
      {isOpen && (
        <ul
          className="absolute text-center shadow-md z-10 border border-blue-800 dark:border-gray-700 bg-white dark:bg-blue-1000 rounded-md"
          style={actionsListStyle}
          data-cy="dropdown-actions"
        >
          {actions.map((action) => (
            <li
              key={action.label}
              className="py-2 px-4 last:mb-0 dark:even:bg-blue-1100 even:bg-gray-200"
              data-cy="dropdown-action"
              data-label={action.label}
            >
              <Button
                size="xs"
                variant="text"
                color="neutral"
                HtmlButtonProps={{
                  type: 'button',
                  onClick: handleClickAction(action),
                }}
              >
                {action.label}
              </Button>
            </li>
          ))}
        </ul>
      )}
    </div>
  );
};

DropdownActions.defaultProps = {
  verticalAlignment: 'bottom',
  horizontalAlignment: 'left',
};
