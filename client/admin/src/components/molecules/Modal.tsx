import { FC, ReactElement, ReactEventHandler } from 'react';

import { IconButton, Title, Card } from '@atoms';

type ModalProps = {
  title: string;
  children: ReactElement | ReactElement[];
  isDisabled?: boolean;
  onClose: () => void;
};

export const Modal: FC<ModalProps> = (props) => {
  const { title, children, isDisabled, onClose } = props;

  const handleClickPrevent: ReactEventHandler = (event) => {
    event.stopPropagation();
  };

  return (
    <div
      role="button"
      tabIndex={-2}
      onClick={onClose}
      onKeyDown={onClose}
      className="fixed z-10 cursor-default left-0 right-0 top-0 bottom-0 overflow-auto flex items-center justify-center flex-wrap bg-opacity-50 bg-black py-8"
    >
      <div
        role="button"
        tabIndex={-1}
        onClick={handleClickPrevent}
        onKeyDown={handleClickPrevent}
        className="cursor-default min-w-[500px]"
      >
        <div className="cursor-auto">
          <Card>
            <div className="py-5 px-8">
              <div className="flex justify-between">
                <div className="mb-10">
                  <Title variant="xs">{title}</Title>
                </div>
                <div className="rotate-45  text-blue-800 dark:text-white -mt-5">
                  <IconButton
                    iconName="plus"
                    onClick={onClose}
                    isDisabled={isDisabled}
                  />
                </div>
              </div>
              {children}
            </div>
          </Card>
        </div>
      </div>
    </div>
  );
};

Modal.defaultProps = {
  isDisabled: false,
};
