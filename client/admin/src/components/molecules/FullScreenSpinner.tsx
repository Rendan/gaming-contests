import { FC } from 'react';

import { Spinner } from '@atoms';

export const FullScreenSpinner: FC = () => {
  return (
    <div className="h-screen w-screen flex items-center justify-center bg-primary">
      <Spinner />
    </div>
  );
};
