import { dictionary } from 'components/atoms/Icon/dictionary';

export type NavigationItem = {
  path: string;
  label: string;
  iconName: keyof typeof dictionary;
};
