import { appRoutes } from 'routes/appRoutes';

import { NavigationItem } from './Navigation.types';

export const navigationItems: Array<NavigationItem> = [
  {
    path: appRoutes.users(),
    label: 'Users',
    iconName: 'user',
  },
  {
    path: appRoutes.profile(),
    label: 'Profile',
    iconName: 'user',
  },
];
