import { Icon, Link } from '@atoms';

import { navigationItems } from './Navigation.constants';

const getLinkClassName = (isActive: boolean) =>
  isActive
    ? 'py-2 block border-r-4 dark:border-purple-100 border-primary text-blue-800 dark:text-white font-bold transition'
    : 'py-2 block text-gray-600 hover:border-r-4 dark:hover:border-purple-100 border-gray-600 transition';

export const Navigation = () => {
  return (
    <ul>
      {navigationItems.map(({ path, label, iconName }) => {
        return (
          <li className="mt-2" key={path}>
            <Link
              to={path}
              className={({ isActive }) => getLinkClassName(isActive)}
            >
              <div className="flex items-center">
                <span className="mr-3">
                  <Icon name={iconName} className="text-inherit" />
                </span>
                {label}
              </div>
            </Link>
          </li>
        );
      })}
    </ul>
  );
};
