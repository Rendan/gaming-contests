import { take } from 'ramda';

import { UserRoleLabel } from 'domain/enum/user';

import { UserRaw } from 'types/responses/api/v1/user';

interface IUser {
  user: UserRaw;
  initials: string;
}

export class User implements IUser {
  user: UserRaw;

  constructor(user: UserRaw) {
    this.user = user;
  }

  get id() {
    return this.user.id;
  }

  get email() {
    return this.user.email;
  }

  get firstName() {
    return this.user.firstName;
  }

  get lastName() {
    return this.user.lastName;
  }

  get role() {
    return this.user.role;
  }

  get roleFormatted() {
    return UserRoleLabel[this.user.role];
  }

  get status() {
    return this.user.status;
  }

  get avatarUrl() {
    return this.user.avatarUrl;
  }

  get initials() {
    return `${take(1, this.user.firstName)}${take(1, this.user.lastName)}`;
  }

  get fullName() {
    return `${this.user.firstName} ${this.user.lastName}`;
  }
}
