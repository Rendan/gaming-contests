export enum UserRole {
  Admin = 'admin',
  Guest = 'guest',
  Manager = 'manager',
  ContestParticipant = 'contest-participant',
}

export enum UserStatus {
  Active = 'active',
  Suspended = 'suspended',
  Disabled = 'disabled',
}

export enum UserStatusLabel {
  active = 'Active',
  suspended = 'Suspended',
  disabled = 'Disabled',
}

export enum UserRoleLabel {
  admin = 'Admin',
  guest = 'Guest',
  manager = 'Manager',
  'contest-participant' = 'Contest Participant',
}
