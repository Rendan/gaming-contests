import { createHttpRequestAtom } from 'utils/store/http';

export const createResourceLoadSingleAtom = createHttpRequestAtom;
