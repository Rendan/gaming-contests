import { createHttpRequestAtom } from 'utils/store/http';

export const createResourceDeletingAtom = createHttpRequestAtom;
