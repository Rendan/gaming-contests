import { createHttpRequestAtom } from 'utils/store/http';

export const createResourceUpdatingAtom = createHttpRequestAtom;
