import {
  createHttpRequestAtom,
  CreateHttpRequestAtomParams,
} from 'utils/store/http';

const appendData = <T extends Nullable<PaginatedData<unknown>>>(
  newData: T,
  previousData: T,
): T => {
  if (!newData) {
    return previousData;
  }

  if (previousData === null || newData.meta.currentPage === 1) {
    return newData;
  }

  const newItems = [...previousData.items, ...newData.items];

  return { items: newItems, meta: newData.meta } as T;
};

export const createResourceLoadPaginatedListAtom = <T, P>(
  params: CreateHttpRequestAtomParams<PaginatedData<T>, P>,
) => {
  return createHttpRequestAtom<PaginatedData<T>, P>({
    serializeResponse: appendData,
    performAction: params.performAction,
    debugLabel: params.debugLabel,
  });
};
