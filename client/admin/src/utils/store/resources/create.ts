import { createHttpRequestAtom } from 'utils/store/http';

export const createResourceCreatingAtom = createHttpRequestAtom;
