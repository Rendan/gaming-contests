export { createResourceCreatingAtom } from './create';
export { createResourceUpdatingAtom } from './update';
export { createResourceDeletingAtom } from './delete';
export { createResourceLoadSingleAtom } from './loadSingle';

export const extractAtomState = <A, P>([atomData, performRequest]: [A, P]) => ({
  ...atomData,
  performRequest,
});
