/* eslint-disable max-params */
import { atom, WritableAtom } from 'jotai';
import { identity } from 'ramda';

import { HttpRequestState } from 'types/enums/http';

export type CreateHttpRequestAtomParams<T, P = Array<unknown>> = {
  debugLabel: string;
  performAction: PerformResourceSaveAction<T, P>;
  serializeResponse?: (
    previousData: Nullable<T>,
    newData: Nullable<T>,
  ) => Nullable<T>;
};

export type HttpRequestAtomState<T> = {
  data: T | null;
  requestState: HttpRequestState;
  error: HttpRequestError<T> | null;
};

type HttpRequestAtomRequestAtom<T, P> = WritableAtom<
  HttpRequestAtomState<T>,
  P,
  Promise<void>
>;
type HttpRequestResetDataAtom<T> = WritableAtom<null, Nullable<T>>;
export type HttpRequestAtom<T, P> = [
  HttpRequestAtomRequestAtom<T, P>,
  HttpRequestResetDataAtom<T>,
];

export const createHttpRequestAtom = <T, P>(
  params: CreateHttpRequestAtomParams<T, P>,
): HttpRequestAtom<T, P> => {
  const { debugLabel, performAction, serializeResponse = identity } = params;

  const requestStateAtom = atom<HttpRequestState>(HttpRequestState.IDLE);
  const dataAtom = atom<Nullable<T>>(null);
  const errorAtom = atom<Nullable<HttpRequestError<T>>>(null);

  requestStateAtom.debugLabel = `${debugLabel}: Request State`;
  dataAtom.debugLabel = `${debugLabel}: Data`;
  errorAtom.debugLabel = `${debugLabel}: Error`;

  const httpRequestAtom: HttpRequestAtomRequestAtom<T, P> = atom(
    (get) => ({
      data: get(dataAtom),
      requestState: get(requestStateAtom),
      error: get(errorAtom),
    }),
    async (get, set, actionParams: P) => {
      try {
        set(requestStateAtom, HttpRequestState.Pending);

        const response = (await performAction(actionParams)) as Awaited<T>;

        const previousDataState = get(dataAtom);
        const newData = serializeResponse(response, previousDataState);

        set(requestStateAtom, HttpRequestState.Success);
        set(dataAtom, newData);
        set(errorAtom, null);
      } catch (error) {
        set(requestStateAtom, HttpRequestState.Fail);
        set(errorAtom, error as HttpRequestError<T>);
      }
    },
  );

  httpRequestAtom.debugLabel = debugLabel;

  const setDataAtom: HttpRequestResetDataAtom<T> = atom(
    null,
    (get, set, newValue) => {
      set(dataAtom, newValue);
    },
  );

  setDataAtom.debugLabel = `${debugLabel}: Data Setter`;

  return [httpRequestAtom, setDataAtom];
};
