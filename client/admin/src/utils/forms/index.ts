import { TextInputField } from 'types/components/input';
import { Option, SelectField } from 'types/components/select';
import { FormFieldTypeEnum } from 'types/enums/ui';

type CreateSelectInputFieldParams<T> = {
  name: keyof T;
  placeholder: string;
  options: Array<Option>;
};

export const createTextInputField = <T>(
  name: keyof T,
  placeholder: string,
): TextInputField<T> => ({
  type: FormFieldTypeEnum.Text,
  name,
  placeholder,
});

export const createSelectInputField = <T>(
  params: CreateSelectInputFieldParams<T>,
): SelectField<T> => ({
  type: 'select',
  ...params,
});

export const createSelectOptionsFromEnum = <
  P extends Record<string, string | number>,
>(
  targetEnum: P,
  includeAllOption = true,
): Array<Option> => {
  const keys = Object.keys(targetEnum);
  const options = keys.map((key) => {
    const value = key;
    const label = targetEnum[key] as string;

    return {
      value,
      label,
    };
  });

  return includeAllOption ? [{ label: 'All', value: '' }, ...options] : options;
};
