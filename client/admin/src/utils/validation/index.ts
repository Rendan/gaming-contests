import { mixed, string } from 'yup';

import { REQUIRED_FIELD } from './errorMessages';

export const requiredStringField = (errorMessage = REQUIRED_FIELD) =>
  string().required(errorMessage);

export const requiredEmailField = (errorMessage = REQUIRED_FIELD) =>
  requiredStringField(errorMessage).email();

export const requiredEnumField = <T>(
  targetEnum: T,
  errorMessage = REQUIRED_FIELD,
) => mixed().oneOf(Object.values(targetEnum)).required(errorMessage);
