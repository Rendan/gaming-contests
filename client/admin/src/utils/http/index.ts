import { StatusCodes } from 'http-status-codes';

import { HttpRequestState } from 'types/enums/http';

export const isUnauthorizedError = (error: HttpRequestError) =>
  error.statusCode === StatusCodes.UNAUTHORIZED;

export const isBadRequestError = (error: HttpRequestError) =>
  error.statusCode === StatusCodes.BAD_REQUEST;

export const hasRequestStarted = (state: HttpRequestState) =>
  state !== HttpRequestState.IDLE;

export const isRequestPending = (state: HttpRequestState) =>
  state === HttpRequestState.Pending;
