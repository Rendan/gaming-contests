export const getFormikErrorsFromHttpRequestError = (
  error: HttpRequestError,
): Record<string, string> => {
  if (typeof error.message === 'string') {
    return {};
  }

  return error.message.reduce(
    (acc, currentErrorItem) => ({
      ...acc,
      [currentErrorItem.property]: Object.values(
        currentErrorItem.constraints,
      ).join(' '),
    }),
    {},
  );
};
