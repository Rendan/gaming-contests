import { filter, isEmpty, not, pipe } from 'ramda';

import { isKeyOfRecord } from './ts';

export const omitNullValuesKeys = (obj: Record<string, unknown>) => {
  return filter(pipe(isEmpty, not), obj);
};

export const createEnumLabelGetter =
  <T extends Record<string, string | number>>(targetEnum: T) =>
  (value: string | number) => {
    if (isKeyOfRecord(value, targetEnum)) {
      return targetEnum[value];
    }

    return null;
  };
