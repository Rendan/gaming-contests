type GetRandomNumberParams = {
  min: number;
  max: number;
  isFloat?: boolean;
};

export const getRandomNumber = (params: GetRandomNumberParams) => {
  const { min, max, isFloat = false } = params;

  const range = max - min;
  const random = Math.random() * range + min;

  return isFloat ? random : Math.floor(random);
};
