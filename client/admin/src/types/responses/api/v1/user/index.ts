import { UserRole, UserStatus } from 'domain/enum/user';

export interface UserRaw {
  id: number;
  firstName: string;
  lastName: string;
  email: string;
  avatarUrl: string;
  status: UserStatus;
  role: UserRole;
}

export interface UserResponse {
  user: UserRaw;
}

export interface UsersResponse {
  users: PaginatedData<UserRaw>;
}

export type UserCreatingPayload = Omit<UserRaw, 'id' | 'avatarUrl'>;

export type UserUpdatingPayload = Omit<UserRaw, 'id'>;
