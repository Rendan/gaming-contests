import { UserRaw } from 'types/responses/api/v1/user';

type ProfileRaw = UserRaw;

export type ProfileResponse = { profile: ProfileRaw };
