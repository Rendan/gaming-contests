export interface SignInResponse {
  accessToken: string;
}

export type RefreshResponse = SignInResponse;

export interface SignInPayload {
  email: string;
  password: string;
}

export interface SignUpPayload {
  firstName: string;
  lastName: string;
  email: string;
  password: string;
  passwordConfirmation: string;
}
