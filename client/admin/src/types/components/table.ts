export type TableColumn<T> = {
  name: keyof T;
  formatValue?: (value: T[keyof T]) => string | number | null;
};

export type TableColumns<T> = Array<TableColumn<T>>;

export type TableHeading<T> = {
  name: keyof T;
  label: string;
  width?: `${number}px`;
};
