export type DropdownAction<T> = {
  label: string;
  onClick: (target: T) => void;
};
