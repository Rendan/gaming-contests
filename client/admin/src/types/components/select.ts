export type Option = {
  label: string;
  value: string;
};

export type SelectField<T> = {
  type: 'select';
  name: keyof T;
  options: Array<Option>;
  placeholder: string;
  label?: string;
  id?: string;
};
