import { FormFieldTypeEnum } from 'types/enums/ui';

export type TextInputField<T> = {
  type: FormFieldTypeEnum.Text | FormFieldTypeEnum.Password;
  name: keyof T;
  placeholder: string;
  label?: string;
  id?: string;
};
