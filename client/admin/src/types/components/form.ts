import { TextInputField } from './input';
import { SelectField } from './select';

export type FormField<T> = TextInputField<T> | SelectField<T>;
