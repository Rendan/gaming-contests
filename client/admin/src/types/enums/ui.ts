export enum Theme {
  dark = 'dark',
  light = 'light',
}

export enum FormFieldTypeEnum {
  Text = 'text',
  Password = 'password',
  Select = 'select',
}
