export enum HttpRequestState {
  IDLE = 'IDLE',
  Pending = 'Pending',
  Success = 'Success',
  Fail = 'Fail',
}
