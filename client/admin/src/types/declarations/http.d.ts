declare interface HttpRequestErrorMessage<P> {
  children: Array<HttpRequestErrorMessage>;
  constraints: Record<string, string>;
  property: keyof P;
  target: Partial<P>;
  value: unknown;
}

declare interface HttpRequestError<P = Record<string, unknown>> {
  statusCode: number;
  message: string | Array<HttpRequestErrorMessage<P>>;
}
