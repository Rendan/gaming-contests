import { SortDirection } from 'types/enums/sorting';

declare global {
  declare namespace NodeJS {
    export interface ProcessEnv {
      SERVER_PROTOCOL: string;
      SERVER_HOSTNAME: string;
      SERVER_PORT: number;
    }
  }

  declare type UnknownRecord = Record<string | number | symbol, unknown>;

  declare type Nullable<T> = null | T;

  declare type ResourceListState = {
    data: Nullable<Array<T>>;
    error: Nullable<HttpRequestError>;
    load: (...params: unknown) => void;
  };

  declare type ResourcePaginatedListState<T> = {
    data: Nullable<Array<T>>;
    meta: Nullable<Meta>;
    error: Nullable<HttpRequestError>;
    loadNextPage: (pageNumber?: number) => void;
  };

  declare type ResourceSingleState<T, P = never> = {
    data: Nullable<T>;
    error: Nullable<HttpRequestError>;
    load: (params: P) => void;
  };

  declare type ResourceCreatingState<T, P = never> = {
    data: Nullable<T>;
    error: Nullable<HttpRequestError<T>>;
    perform: (params: P) => void;
  };

  declare type ResourceUpdatingState<T, P = never> = {
    data: Nullable<T>;
    error: Nullable<HttpRequestError<T>>;
    perform: (params: P) => void;
  };

  declare type ResourceDeletingState = {
    data: Nullable<string>;
    error: Nullable<HttpRequestError>;
    perform: PerformResourceDeleteAction;
  };

  declare type PerformResourceSaveAction<T, P> = (
    params: P,
  ) => Promise<T | HttpRequestError<T>>;

  declare type PerformResourceDeleteAction = (
    id: number,
  ) => Promise<string | HttpRequestError>;

  declare type PerformResourceLoadSingleAction<T, P> = (
    params: P,
  ) => Promise<T | HttpRequestError>;

  declare type PerformResourceLoadPaginatedListAction<T, P> = (
    params: P,
  ) => Promise<PaginatedData<T> | HttpRequestError>;

  declare type PaginatedData<T> = {
    items: Array<T>;
    meta: Meta;
  };

  declare type PaginatedResponseData<T> = {
    items: Array<T>;
    meta: Meta;
  };

  declare type Meta = {
    currentPage: number;
    itemCount: number;
    itemsPerPage: number;
    totalItems: number;
    totalPages: number;
  };

  declare type MetaPayload = {
    page?: number;
    limit?: number;
  };

  declare type SortingPayload<T> = {
    sortBy: keyof T;
    sortDirection: SortDirection;
  };

  declare type PaginatedDataLoadingParams<T> = Partial<MetaPayload> &
    Partial<Record<keyof T, unknown>> &
    Partial<SortingPayload<T>>;
}
