import { User } from 'domain/models';

import {
  UserResponse,
  UserCreatingPayload,
  UsersResponse,
  UserRaw,
} from 'types/responses/api/v1/user';

import { httpService } from 'services/http';

import { apiRoutes } from 'routes/apiRoutes';

export const createUser = async (payload: UserCreatingPayload) => {
  const { user } = await httpService.post<UserCreatingPayload, UserResponse>(
    apiRoutes.apiV1Users(),
    payload,
  );

  return new User(user);
};

export const updateUser = async ({
  id,
  payload,
}: {
  id: number;
  payload: UserCreatingPayload;
}) => {
  const { user } = await httpService.patch<UserCreatingPayload, UserResponse>(
    apiRoutes.apiV1User(id),
    payload,
  );

  return new User(user);
};

export const deleteUser = (userId: number) => {
  return httpService.delete<string>(apiRoutes.apiV1User(userId));
};

export const loadById = async (userId: number) => {
  const { user } = await httpService.get<UserResponse>(
    apiRoutes.apiV1User(userId),
  );

  return new User(user);
};

export const loadPaginatedList = async (
  params?: PaginatedDataLoadingParams<UserRaw>,
) => {
  const { users } = await httpService.get<
    UsersResponse,
    PaginatedDataLoadingParams<UserRaw>
  >(apiRoutes.apiV1Users(), params);
  const { items, meta } = users;

  return {
    items: items.map((user) => new User(user)),
    meta,
  };
};
