import { Profile } from 'domain/models';

import { ProfileResponse } from 'types/responses/api/v1/profile';

import { httpService } from 'services/http';

import { apiRoutes } from 'routes/apiRoutes';

export const loadProfile = async () => {
  const { profile } = await httpService.get<ProfileResponse>(
    apiRoutes.apiV1Profile(),
  );

  return new Profile(profile);
};
