import {
  SignInPayload,
  SignInResponse,
  SignUpPayload,
} from 'types/responses/api/v1/auth';

import { httpService } from 'services/http';

import { apiRoutes } from 'routes/apiRoutes';

export const signIn = (payload: SignInPayload) => {
  return httpService.post<SignInPayload, SignInResponse>(
    apiRoutes.apiV1AuthSignIn(),
    payload,
  );
};

export const signUp = (payload: SignUpPayload) => {
  return httpService.post<SignUpPayload, string>(
    apiRoutes.apiV1AuthSignUp(),
    payload,
  );
};
