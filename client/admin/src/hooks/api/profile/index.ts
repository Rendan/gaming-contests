import { profileAtom } from 'store/resources/api/v1/profile';

import { useHttpRequestAtom } from 'hooks/http';

export const useProfile = () => {
  const { data, performRequest, ...rest } = useHttpRequestAtom(profileAtom);

  return {
    profile: data,
    loadProfile: performRequest,
    ...rest,
  };
};
