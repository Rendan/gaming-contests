import { useCallback, useMemo } from 'react';

import {
  userCreatingAtom,
  userDeletingAtom,
  userLoadingAtom,
  usersLoadingAtom,
  userUpdatingAtom,
} from 'store/resources/api/v1/users';

import { useHttpRequestAtom } from 'hooks/http';

export const useUsersCreation = () => {
  const { data, performRequest, ...rest } =
    useHttpRequestAtom(userCreatingAtom);

  return {
    user: data,
    createUser: performRequest,
    ...rest,
  };
};

export const useUsersUpdating = () => {
  const { data, performRequest, ...rest } =
    useHttpRequestAtom(userUpdatingAtom);

  return {
    user: data,
    updateUser: performRequest,
    ...rest,
  };
};

export const useUsersDeleting = () => {
  const { data, performRequest, ...rest } =
    useHttpRequestAtom(userDeletingAtom);

  return {
    deleteUser: performRequest,
    ...rest,
  };
};

export const useUser = () => {
  const { data, performRequest, ...rest } = useHttpRequestAtom(userLoadingAtom);

  return {
    user: data,
    loadUser: performRequest,
    ...rest,
  };
};

export const useUsers = () => {
  const atom = useHttpRequestAtom(usersLoadingAtom);
  const { data, performRequest, ...rest } = useMemo(() => atom, [atom]);

  const handleLoadMore = useCallback(
    async (payload: MetaPayload) => {
      if (!data) {
        return;
      }

      await performRequest({
        ...payload,
        page: data.meta.currentPage + 1,
      });
    },
    [data?.meta, performRequest],
  );

  return {
    users: data,
    loadUsers: performRequest,
    loadMore: handleLoadMore,
    ...rest,
  };
};
