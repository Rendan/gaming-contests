import { useLocation } from 'react-router-dom';

export const useLocationState = <T>() => {
  const { state } = useLocation();

  return state as T | null;
};
