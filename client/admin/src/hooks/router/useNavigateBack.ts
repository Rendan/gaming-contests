import { useMemo } from 'react';
import { useNavigate } from 'react-router-dom';

export const useNavigateBack = () => {
  const navigate = useNavigate();

  return useMemo(() => () => navigate(-1), []);
};
