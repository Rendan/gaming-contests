import { useMemo } from 'react';
import { useNavigate } from 'react-router-dom';

export const useNavigateTo = (route: string) => {
  const navigate = useNavigate();

  return useMemo(() => () => navigate(route), [route]);
};
