import { useAtom } from 'jotai';
import { useCallback } from 'react';

import { HttpRequestAtom } from 'utils/store/http';
import { extractAtomState } from 'utils/store/resources';

export const useHttpRequestAtom = <T, P>(atoms: HttpRequestAtom<T, P>) => {
  const [httpRequestAtom, setDataAtom] = atoms;
  const { data, performRequest, ...rest } = extractAtomState(
    useAtom(httpRequestAtom),
  );
  const [, setData] = useAtom(setDataAtom);

  const resetData = useCallback(() => {
    setData(null);
  }, [setData]);

  return {
    data,
    performRequest,
    resetData,
    setData,
    ...rest,
  };
};
