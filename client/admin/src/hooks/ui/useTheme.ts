import { useAtom } from 'jotai';
import { useCallback } from 'react';

import { Theme } from 'types/enums/ui';

import { themeAtom } from 'store/ui/theme';

export const useTheme = () => {
  const [theme, setTheme] = useAtom(themeAtom);

  const toggleTheme = useCallback(() => {
    setTheme(theme === Theme.dark ? Theme.light : Theme.dark);
  }, [theme]);

  return {
    theme,
    toggleTheme,
  };
};
