import { useCallback, useState } from 'react';

import { SortDirection } from 'types/enums/sorting';

export const useSorting = <T>(
  name: keyof T,
  initialSortDirection: SortDirection = SortDirection.DESC,
) => {
  const [sortBy, setSortBy] = useState(name);
  const [sortDirection, setSortDirection] = useState(initialSortDirection);

  const changeSorting: React.MouseEventHandler<HTMLButtonElement> = useCallback(
    (event) => {
      const newSortBy = event.currentTarget.name as keyof T;

      if (sortBy === newSortBy) {
        setSortDirection(
          sortDirection === SortDirection.ASC
            ? SortDirection.DESC
            : SortDirection.ASC,
        );

        return;
      }

      setSortBy(newSortBy);
    },
    [sortDirection, sortBy, setSortBy, setSortDirection],
  );

  return { sortBy, sortDirection, changeSorting };
};
