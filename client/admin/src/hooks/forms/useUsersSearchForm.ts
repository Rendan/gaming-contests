import { useFormik } from 'formik';

import { User } from 'domain/models';

import { noop } from 'utils';

export type SearchFormValues<T> = Partial<Record<keyof Partial<T>, string>>;

export type UsersSearchFormValues = SearchFormValues<User>;

const INITIAL_VALUES: UsersSearchFormValues = {
  id: '',
  email: '',
  firstName: '',
  lastName: '',
  role: '',
  status: '',
};

export const useUsersSearchForm = () => {
  const { values, handleChange } = useFormik({
    initialValues: INITIAL_VALUES,
    onSubmit: noop,
  });

  return { values, handleChange };
};
