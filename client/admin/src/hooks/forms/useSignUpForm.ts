import { FormikHelpers, useFormik } from 'formik';

import { schema } from 'forms/auth/signup/schema';
import { SignUpFormValues } from 'forms/auth/signup/types';
import { getInitialValues } from 'forms/auth/signup/values';

const initialValues = getInitialValues();

interface UseSignUpFormParams {
  onSubmit: (
    values: SignUpFormValues,
    formikHelpers: FormikHelpers<SignUpFormValues>,
  ) => Promise<void>;
}

export const useSignUpForm = (params: UseSignUpFormParams) => {
  const { onSubmit } = params;

  const { values, touched, errors, isSubmitting, handleSubmit, handleChange } =
    useFormik({
      initialValues,
      validationSchema: schema,
      validateOnChange: true,
      validateOnBlur: false,
      onSubmit,
    });

  return {
    values,
    touched,
    errors,
    isSubmitting,
    handleSubmit,
    handleChange,
  };
};
