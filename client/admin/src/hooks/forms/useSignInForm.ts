import { FormikHelpers, useFormik } from 'formik';

import { SignInForm } from 'forms/auth/signin/types';
import { schema } from 'forms/auth/signin/schema';
import { getInitialValues } from 'forms/auth/signin/values';

const initialValues = getInitialValues();

interface UseSignInFormParams {
  onSubmit: (
    values: SignInForm,
    formikHelpers: FormikHelpers<SignInForm>,
  ) => Promise<void>;
}

export const useSignInForm = (params: UseSignInFormParams) => {
  const { onSubmit } = params;

  const { values, touched, errors, isSubmitting, handleSubmit, handleChange } =
    useFormik({
      initialValues,
      validationSchema: schema,
      validateOnChange: true,
      validateOnBlur: false,
      onSubmit,
    });

  return { values, touched, errors, isSubmitting, handleSubmit, handleChange };
};
