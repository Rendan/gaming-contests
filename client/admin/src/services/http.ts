import axios, { AxiosError, AxiosInstance, AxiosResponse } from 'axios';

import { RefreshResponse } from 'types/responses/api/v1/auth';

import { signIn } from 'repositories/api/v1/auth';

import { SignInForm } from 'forms/auth/signin/types';

import { apiRoutes } from 'routes/apiRoutes';
import { appRoutes } from 'routes/appRoutes';

import { isUnauthorizedError } from '../utils/http/index';

const HTTP_HEADERS = {
  'Content-Type': 'application/json',
};

export type QueryParams = Record<string, unknown>;

const axiosInstance = axios.create({ headers: HTTP_HEADERS });

axiosInstance.interceptors.response.use(
  (axiosResponse: AxiosResponse<unknown, unknown>) => axiosResponse.data,
  (axiosError: AxiosError<HttpRequestError, unknown>) =>
    Promise.reject(axiosError.response?.data),
);

export class HttpService {
  axiosInstance: AxiosInstance;

  constructor() {
    this.axiosInstance = axios.create({ headers: HTTP_HEADERS });
    this.axiosInstance.interceptors.response.use(
      (axiosResponse: AxiosResponse<unknown, unknown>) => axiosResponse.data,
      async (axiosError: AxiosError<HttpRequestError, unknown>) => {
        const httpRequestError = axiosError.response?.data;

        const isRefreshTokenRequestFailed =
          axiosError.config.url === apiRoutes.apiV1AuthRefresh();

        const shouldRefreshToken =
          httpRequestError &&
          isUnauthorizedError(httpRequestError) &&
          !isRefreshTokenRequestFailed;

        if (!shouldRefreshToken) {
          return Promise.reject(httpRequestError);
        }

        try {
          await this.refreshToken();

          return await this.axiosInstance(axiosError.config);
        } catch (error) {
          window.location.href = appRoutes.signIn();
        }

        return Promise.reject(httpRequestError);
      },
    );
  }

  setHeader(headerName: string, value: string) {
    this.axiosInstance.defaults.headers.common = {
      ...axiosInstance.defaults.headers.common,
      [headerName]: value,
    };
  }

  async refreshToken() {
    const refreshTokenResponse = await this.post<undefined, RefreshResponse>(
      apiRoutes.apiV1AuthRefresh(),
      undefined,
    );

    this.setAuthorizationHeader(refreshTokenResponse.accessToken);
  }

  async authorize(credentials: SignInForm) {
    const { accessToken } = await signIn(credentials);

    this.setAuthorizationHeader(accessToken);
  }

  private setAuthorizationHeader(accessToken: string) {
    this.setHeader('Authorization', `Bearer ${accessToken}`);
  }

  get<D = unknown, P = QueryParams>(url: string, params?: P) {
    return this.axiosInstance.get<P, D>(url, { params });
  }

  post<P, D>(url: string, params: P) {
    return this.axiosInstance.post<P, D>(url, params);
  }

  patch<P, D>(url: string, params?: QueryParams) {
    return this.axiosInstance.patch<P, D>(url, params);
  }

  delete<D>(url: string) {
    return this.axiosInstance.delete<D, string>(url);
  }
}

export const httpService = new HttpService();
