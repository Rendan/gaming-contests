export const appRoutes = {
  all: () => '*',
  root: () => '/',
  signIn: () => '/sign-in',
  signUp: () => '/sign-up',
  forgotPassword: () => '/forgot-password',
  users: () => '/users',
  usersNew: () => `/users/new`,
  user: (id: number | string) => `/users/${id}`,
  userEdit: (id?: number | string) => `/users/${id || ':userId'}/edit`,
  userDelete: (id?: number | string) => `/users/${id || ':userId'}/delete`,
  profile: () => '/profile',
} as const;
