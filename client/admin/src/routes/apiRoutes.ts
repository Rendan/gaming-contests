export const apiRoutes = {
  apiV1Profile: () => '/api/v1/profile',
  apiV1Users: () => '/api/v1/users',
  apiV1User: (id: number) => `/api/v1/users/${id}`,
  apiV1AuthSignIn: () => '/api/v1/auth/sign-in',
  apiV1AuthSignUp: () => '/api/v1/auth/sign-up',
  apiV1AuthRefresh: () => '/api/v1/auth/refresh',
} as const;
