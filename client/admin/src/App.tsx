import {
  BrowserRouter,
  Routes as ReactRouterRoutes,
  Route,
  Navigate,
} from 'react-router-dom';
import { useEffect, useState } from 'react';
import { useAtomsDevtools } from 'jotai/devtools';

import { httpService } from 'services/http';

import { useProfile } from 'hooks/api/profile';
import { useTheme } from 'hooks/ui/useTheme';

import { hasRequestStarted, isRequestPending } from 'utils/http';

import { FullScreenSpinner } from '@molecules';

import { router, Routes } from './router';

const renderRoutes = (routes: Routes) => {
  return routes.map(({ Page, path, children }) => (
    <Route key={path} path={path} element={Page}>
      {children && renderRoutes(children)}
    </Route>
  ));
};

const App = () => {
  const [isRefreshTokenFailed, setRefreshTokenFailed] = useState(false);
  const { profile, loadProfile, requestState } = useProfile();

  const { theme } = useTheme();

  useAtomsDevtools('Sovereign');

  useEffect(() => {
    if (!profile) {
      httpService
        .refreshToken()
        .then(loadProfile)
        .catch(() => {
          setRefreshTokenFailed(true);
        });
    }
  }, [profile, loadProfile]);

  if (isRequestPending(requestState)) {
    return <FullScreenSpinner />;
  }

  if (!hasRequestStarted(requestState) && !isRefreshTokenFailed) {
    return null;
  }

  return (
    <div data-cy="theme-container" className={theme}>
      <BrowserRouter>
        <ReactRouterRoutes>
          {profile && (
            <>
              {renderRoutes(router.authorized.routes)}
              {router.authorized.redirects.map(({ from, to }) => (
                <Route key={from} path={from} element={<Navigate to={to} />} />
              ))}
            </>
          )}
          {!profile && (
            <>
              {renderRoutes(router.notAuthorized.routes)}
              {router.notAuthorized.redirects.map(({ from, to }) => (
                <Route key={from} path={from} element={<Navigate to={to} />} />
              ))}
            </>
          )}
        </ReactRouterRoutes>
      </BrowserRouter>
    </div>
  );
};

export default App;
