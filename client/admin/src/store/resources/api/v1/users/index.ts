import {
  createUser,
  deleteUser,
  loadById,
  loadPaginatedList,
  updateUser,
} from 'repositories/api/v1/users';

import { createHttpRequestAtom } from 'utils/store/http';
import { createResourceLoadPaginatedListAtom } from 'utils/store/resources/loadPaginatedList';

export const userCreatingAtom = createHttpRequestAtom({
  performAction: createUser,
  debugLabel: 'Create User',
});

export const userUpdatingAtom = createHttpRequestAtom({
  performAction: updateUser,
  debugLabel: 'Update User',
});

export const userDeletingAtom = createHttpRequestAtom({
  performAction: deleteUser,
  debugLabel: 'Update Delete',
});

export const userLoadingAtom = createHttpRequestAtom({
  performAction: loadById,
  debugLabel: 'User',
});

export const usersLoadingAtom = createResourceLoadPaginatedListAtom({
  performAction: loadPaginatedList,
  debugLabel: 'Users List',
});
