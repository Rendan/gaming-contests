import { loadProfile } from 'repositories/api/v1/profile';

import { createResourceLoadSingleAtom } from 'utils/store/resources';

export const profileAtom = createResourceLoadSingleAtom({
  performAction: loadProfile,
  debugLabel: "Current User's Profile",
});
