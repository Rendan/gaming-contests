/* eslint-disable max-params */
import { atom } from 'jotai';

import { Theme } from 'types/enums/ui';

export const themeAtom = atom<Theme, Theme>(
  (localStorage.getItem('theme') as Theme) || Theme.light,
  (_, set, theme) => {
    localStorage.setItem('theme', theme);
    set(themeAtom, theme);
  },
);

themeAtom.debugLabel = 'Theme';
