import { appRoutes } from 'routes/appRoutes';

import { Profile, SignIn, SignUp, Users } from '@pages';

type RouteItem = {
  path: string;
  Page: JSX.Element;
};

type Redirect = {
  from: string;
  to: string;
};

export type RouteDeclaration = {
  children?: Routes;
} & RouteItem;

export type Routes = Array<RouteDeclaration>;
export type Redirects = Array<Redirect>;

type Router = {
  authorized: {
    routes: Routes;
    redirects: Redirects;
  };
  notAuthorized: {
    routes: Routes;
    redirects: Redirects;
  };
};

export const router: Router = {
  authorized: {
    routes: [
      {
        Page: <Users.List />,
        path: appRoutes.users(),
        children: [
          {
            Page: <div>New User</div>,
            path: appRoutes.usersNew(),
          },
        ],
      },
      {
        path: appRoutes.user(':id'),
        Page: <Users.Details />,
        children: [
          {
            Page: <Users.Edit />,
            path: appRoutes.userEdit(':id'),
          },
          {
            Page: <Users.Delete />,
            path: appRoutes.userDelete(':id'),
          },
        ],
      },
      {
        path: appRoutes.profile(),
        Page: <Profile />,
      },
    ],
    redirects: [
      {
        from: appRoutes.all(),
        to: appRoutes.users(),
      },
    ],
  },
  notAuthorized: {
    routes: [
      {
        path: appRoutes.signIn(),
        Page: <SignIn />,
      },
      {
        path: appRoutes.signUp(),
        Page: <SignUp />,
      },
    ],
    redirects: [
      {
        from: appRoutes.all(),
        to: appRoutes.signIn(),
      },
    ],
  },
};
