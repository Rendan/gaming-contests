/* eslint-disable import/no-extraneous-dependencies */
import { defineConfig } from 'vite';
import react from '@vitejs/plugin-react';
import svgr from 'vite-plugin-svgr';

const proxyTarget = `${process.env.SERVER_PROTOCOL}://${process.env.SERVER_HOSTNAME}:${process.env.SERVER_PORT}`;

export default defineConfig(() => ({
  plugins: [react(), svgr()],
  root: './src',
  server: {
    host: '0.0.0.0',
    proxy: {
      '/api': {
        target: proxyTarget,
      },
    },
  },
  resolve: {
    alias: {
      '@atoms': `${process.cwd()}/src/components/atoms`,
      '@molecules': `${process.cwd()}/src/components/molecules`,
      '@organisms': `${process.cwd()}/src/components/organisms`,
      '@templates': `${process.cwd()}/src/components/templates`,
      '@pages': `${process.cwd()}/src/components/pages`,
      domain: `${process.cwd()}/src/domain`,
      assets: `${process.cwd()}/src/assets`,
      routes: `${process.cwd()}/src/routes`,
      forms: `${process.cwd()}/src/forms`,
      hooks: `${process.cwd()}/src/hooks`,
      repositories: `${process.cwd()}/src/repositories`,
      store: `${process.cwd()}/src/store`,
      services: `${process.cwd()}/src/services`,
      utils: `${process.cwd()}/src/utils`,
      types: `${process.cwd()}/src/types`,
    },
  },
}));
