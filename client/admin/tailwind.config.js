const defaultTheme = require('tailwindcss/defaultTheme');

module.exports = {
  content: ['./index.html', './src/**/*.tsx'],
  darkMode: 'class',
  mode: 'jit',
  theme: {
    extend: {
      variants: {
        extend: {
          size: ['xs', 'md'],
        },
      },
      fontFamily: {
        sans: ['DM Sans', ...defaultTheme.fontFamily.sans],
        poppins: ['Poppins', ...defaultTheme.fontFamily.sans],
      },
      width: {
        410: '25.625rem',
      },
      maxWidth: {
        410: '25.625rem',
      },
      colors: {
        primary: '#4318FF',
        blue: {
          500: '#4318FF',
          800: '#2B3674',
          900: '#11047A',
          1000: '#111C44',
          1100: '#0B1437',
        },
        gray: {
          200: '#F4F7FE',
          300: '#E9EDF7',
          500: '#E0E5F2',
          600: '#A3AED0',
        },
        purple: {
          100: '#7551FF',
          200: '#868cff',
          300: '#4318ff',
          400: '#5f3de2',
        },
        red: {
          900: '#EE5D50',
        },
      },
    },
  },
  plugins: [],
  corePlugins: {
    float: false,
  },
};
