module.exports = {
  parser: '@typescript-eslint/parser',
  parserOptions: {
    project: 'tsconfig.json',
    tsconfigRootDir: __dirname,
    sourceType: 'module',
  },
  plugins: ['@typescript-eslint/eslint-plugin', 'eslint-plugin-import'],
  extends: [
    'plugin:@typescript-eslint/recommended',
    'plugin:@typescript-eslint/recommended-requiring-type-checking',
    'plugin:prettier/recommended',
  ],
  root: true,
  env: {
    node: true,
    jest: true,
  },
  ignorePatterns: ['.eslintrc.js'],
  rules: {
    '@typescript-eslint/interface-name-prefix': 'off',
    '@typescript-eslint/explicit-function-return-type': 'off',
    '@typescript-eslint/explicit-module-boundary-types': 'off',
    'import/order': [
      'error',
      {
        'newlines-between': 'always',
        groups: [
          'external',
          'builtin',
          'internal',
          'parent',
          'sibling',
          'index',
          'object',
          'type',
        ],
        pathGroups: [
          {
            pattern: '@nestjs/**',
            group: 'external',
            position: 'before',
          },
          {
            pattern: '~/**',
            group: 'external',
          },
          {
            pattern: 'seeds/**',
            group: 'internal',
            position: 'after',
          },
          {
            pattern: 'settings/**',
            group: 'internal',
            position: 'after',
          },
          {
            pattern: 'routes',
            group: 'internal',
            position: 'after',
          },
          {
            pattern: 'domain/**',
            group: 'internal',
            position: 'after',
          },
          {
            pattern: 'types/**',
            group: 'internal',
            position: 'after',
          },
          {
            pattern: 'api/**',
            group: 'internal',
            position: 'after',
          },
          {
            pattern: 'dto/**',
            group: 'internal',
            position: 'after',
          },
          {
            pattern: 'decorators/**',
            group: 'internal',
            position: 'after',
          },
          {
            pattern: 'utils/**',
            group: 'internal',
            position: 'after',
          },
          {
            pattern: './*.controller',
            group: 'sibling',
            position: 'before',
          },
          {
            pattern: './*.service',
            group: 'sibling',
            position: 'before',
          },
          {
            pattern: './entities/**',
            group: 'sibling',
            position: 'after',
          },
          {
            pattern: './dto/**',
            group: 'sibling',
            position: 'after',
          },
        ],
        pathGroupsExcludedImportTypes: ['@nestjs/**'],
      },
    ],
  },
};
