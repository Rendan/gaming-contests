import { INestApplication } from '@nestjs/common';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';

import { PaginatedDto } from 'dto/paginated.dto';

export const setUpSwaggerModule = (app: INestApplication) => {
  const config = new DocumentBuilder()
    .setTitle('Sovereign')
    .setDescription('API description')
    .setVersion('0.0.1')
    .addBearerAuth({ type: 'http', scheme: 'bearer', bearerFormat: 'JWT' })
    .addCookieAuth('refreshToken', {
      type: 'http',
    })
    .build();

  const document = SwaggerModule.createDocument(app, config, {
    extraModels: [PaginatedDto],
  });

  SwaggerModule.setup('api/docs', app, document, {
    swaggerOptions: {
      persistAuthorization: true,
    },
  });
};
