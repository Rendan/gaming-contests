import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';

import * as joi from 'joi';

import { Config } from 'types/config';

import { ConfigService } from './config.service';

@Module({
  imports: [
    ConfigModule.forRoot({
      validationSchema: joi.object<Config>({
        REFRESH_TOKEN_SECRET: joi.string().required(),
        REFRESH_TOKEN_EXPIRATION: joi.string().required(),
        JWT_SECRET: joi.string().required(),
      }),
    }),
  ],
  exports: [ConfigService],
  providers: [ConfigService],
})
export class ServicesModule {}
