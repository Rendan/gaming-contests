import { Injectable } from '@nestjs/common';
import { ConfigService as DefaultConfigService } from '@nestjs/config';
import { JwtModuleOptions } from '@nestjs/jwt';

import { Config, EnvironmentVariablesNames } from 'types/config';

@Injectable()
export class ConfigService {
  constructor(private defaultConfigService: DefaultConfigService<Config>) {}

  static jwtModuleOptions: JwtModuleOptions = {
    secret: process.env[EnvironmentVariablesNames.JWT_SECRET],
    signOptions: { expiresIn: '1d' },
  };

  getRefreshTokenExpirationTime = this.makeGetter(EnvironmentVariablesNames.REFRESH_TOKEN_EXPIRATION);

  getRefreshTokenSecret = this.makeGetter(EnvironmentVariablesNames.REFRESH_TOKEN_SECRET);

  private makeGetter(configParameter: keyof Config): () => string {
    return () => {
      return this.defaultConfigService.get(configParameter);
    };
  }
}
