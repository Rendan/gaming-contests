import { UserRole, UserStatus } from 'domain/enum/user.enum';

export class UserModel {
  id: number;
  firstName: string;
  lastName: string;
  email: string;
  password: string;
  avatarUrl: string;
  refreshToken?: string;
  role: UserRole;
  status: UserStatus;
}
