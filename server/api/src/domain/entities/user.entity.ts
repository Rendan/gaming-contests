import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';
import { Exclude } from 'class-transformer';
import { Factory } from 'nestjs-seeder';
import { faker } from '@faker-js/faker';

import { UserRole, UserStatus } from 'domain/enum/user.enum';
import { UserModel } from 'domain/models/user.model';

import { encryptPassword } from 'utils/password';

@Entity()
export class User implements UserModel {
  @PrimaryGeneratedColumn()
  id: number;

  @Factory(() => faker.name.firstName())
  @Column()
  firstName: string;

  @Factory(() => faker.name.lastName())
  @Column()
  lastName: string;

  @Factory(() => faker.internet.email())
  @Column({ unique: true })
  email: string;

  @Factory(encryptPassword('password'))
  @Column()
  @Exclude()
  password: string;

  @Column({ nullable: true })
  avatarUrl: string;

  @Factory(UserRole.GUEST)
  @Column({
    type: 'enum',
    enum: UserRole,
    default: UserRole.GUEST,
  })
  role: UserRole;

  @Factory(UserStatus.ACTIVE)
  @Column({
    type: 'enum',
    enum: UserStatus,
    default: UserStatus.ACTIVE,
  })
  status: UserStatus;

  @Factory(() => null)
  @Column({ nullable: true })
  @Exclude()
  refreshToken?: string;
}
