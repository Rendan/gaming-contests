export enum UserMessages {
  AlreadyExistsByEmail = 'The user with a given email already exists',
  NotFoundById = 'There is no user with a given ID',
}
