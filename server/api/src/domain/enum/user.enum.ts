export enum UserRole {
  ADMIN = 'admin',
  MANAGER = 'manager',
  CONTEST_PARTICIPANT = 'contest-participant',
  GUEST = 'guest',
}

export enum UserStatus {
  ACTIVE = 'active',
  SUSPENDED = 'suspended',
  DISABLED = 'disabled',
}
