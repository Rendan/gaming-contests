import * as bcrypt from 'bcrypt';
import { PasswordMeter } from 'password-meter';

export const getPasswordStrengthErrors = (password: string) => {
  return new PasswordMeter({
    minLength: 5,
    maxLength: 20,
    uppercaseLettersMinLength: 1,
    lowercaseLettersMinLength: 2,
    numbersMinLength: 1,
    symbolsMinLength: 1,
  }).getResult(password).errors;
};

export const encrypt = (password: string, saltOrRounds: string | number) => bcrypt.hashSync(password, saltOrRounds);

export const encryptPassword = (password: string) => encrypt(password, 10);
export const encryptJwtToken = (password: string) => encrypt(password, 10);
