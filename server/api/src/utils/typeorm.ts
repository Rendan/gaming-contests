import { map } from 'ramda';
import { FindConditions, ILike } from 'typeorm';

import { omitNullProperties } from './object';

export const getILikeWhereFindOptions = <T>(findConditions: FindConditions<T>) => {
  const nonEmptySearchCriteria = omitNullProperties(findConditions);
  const findOptions = map<FindConditions<T>, Record<keyof FindConditions<T>, ReturnType<typeof ILike>>>(
    (value) => ILike(`%${String(value)}%`),
    nonEmptySearchCriteria,
  );
  return findOptions;
};
