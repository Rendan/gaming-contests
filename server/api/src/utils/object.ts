import { filter, isNil, not, pipe } from 'ramda';

export const omitNullProperties = <T>(obj: T) => {
  const isNotNull = pipe(isNil, not);
  const objectWithoutNullValues = filter(isNotNull, obj);

  return objectWithoutNullValues;
};
