import { ApiResponseOptions, getSchemaPath } from '@nestjs/swagger';

import { DtoLabel } from 'domain/enum/dto-label.enum';

export const getDtoPropertyLabel = (property: string) => {
  const label = DtoLabel[property] as string;

  return label || property;
};

// eslint-disable-next-line @typescript-eslint/ban-types
export const getWrappedDtoSchema = <T extends string | Function>(
  wrapperKey: string,
  schema: T,
): ApiResponseOptions => ({
  schema: {
    properties: {
      [wrapperKey]: {
        $ref: getSchemaPath(schema),
      },
    },
  },
});
