import { Module } from '@nestjs/common';
import { RouterModule } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigModule } from '@nestjs/config';

import * as Joi from 'joi';

import { typeormSettings } from 'settings/typeorm.settings';

import { routes } from 'routes';

import { Config } from 'types/config';

import { AuthModule } from 'api/auth/auth.module';
import { UsersModule } from 'api/users/users.module';
import { ProfileModule } from 'api/profile/profile.module';

@Module({
  imports: [
    TypeOrmModule.forRoot(typeormSettings),
    ConfigModule.forRoot({
      validationSchema: Joi.object<Config>({
        REFRESH_TOKEN_SECRET: Joi.string().required(),
        REFRESH_TOKEN_EXPIRATION: Joi.string().required(),
      }),
    }),
    AuthModule,
    ProfileModule,
    UsersModule,
    RouterModule.register(routes),
  ],
})
export class AppModule {}
