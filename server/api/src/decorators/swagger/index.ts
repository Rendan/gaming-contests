/* eslint-disable @typescript-eslint/ban-types */
import { DefaultValuePipe, ParseIntPipe, Query } from '@nestjs/common';
import { ApiQuery } from '@nestjs/swagger';
import { SwaggerEnumType } from '@nestjs/swagger/dist/types/swagger-enum.type';

import { SortDirection } from 'types/enum/sort';

export const ApiQueryLimit = () => {
  return ApiQuery({
    name: 'limit',
    description: 'Records per Page',
    required: false,
  });
};

export const ApiQueryPage = () => {
  return ApiQuery({
    name: 'page',
    description: 'Page Number',
    required: false,
  });
};

export const ApiOptionalEntityEnumProperty = <T>(
  name: keyof T,
  enumName: string,
  entityEnum: SwaggerEnumType,
  description: string,
) => {
  return ApiQuery({
    name: String(name),
    enumName,
    enum: entityEnum,
    description,
    required: false,
  });
};

export const ApiSortBy = <T>(entityEnum: Partial<Record<keyof T, unknown>>) => {
  return ApiQuery({
    enumName: 'SortingEnum',
    enum: entityEnum,
    name: 'sortBy',
    description: 'Sort by a given property',
    required: false,
  });
};

export const ApiSortDirection = () => {
  return ApiQuery({
    enumName: 'SortDirection',
    enum: SortDirection,
    name: 'sortDirection',
    description: 'Sort direction',
    required: false,
  });
};

export const ApiOptionalEntityStringProperty = <T>(name: keyof T, description: string) => {
  return ApiQuery({
    name: String(name),
    description,
    required: false,
  });
};

export const QueryEntityNumberProperty = <T>(name: keyof T) => Query(String(name), new DefaultValuePipe(null));
export const QueryEntityStringProperty = <T>(name: keyof T) => Query(String(name), new DefaultValuePipe(null));
export const QueryEntityEnumProperty = <T>(name: keyof T) => Query(String(name), new DefaultValuePipe(null));

export const QueryPage = () => Query('page', new DefaultValuePipe(1), ParseIntPipe);

export const QuerySortBy = () => Query('sortBy', new DefaultValuePipe('id'));
export const QuerySortDirection = () => Query('sortDirection', new DefaultValuePipe(SortDirection.DESC));

export const QueryLimit = () => Query('limit', new DefaultValuePipe(10), ParseIntPipe);
