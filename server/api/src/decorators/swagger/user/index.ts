import { ApiProperty, ApiPropertyOptions } from '@nestjs/swagger';

import { UserRole, UserStatus } from 'domain/enum/user.enum';

export const ApiPropertyUserRole = (options?: ApiPropertyOptions) =>
  ApiProperty({
    name: 'role',
    enum: UserRole,
    enumName: 'UserRole',
    ...options,
  });

export const ApiPropertyUserStatus = (options?: ApiPropertyOptions) =>
  ApiProperty({
    name: 'status',
    enum: UserStatus,
    enumName: 'UserStatus',
    ...options,
  });
