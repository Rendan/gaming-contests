import { registerDecorator } from 'class-validator';

import { getPasswordStrengthErrors } from 'utils/password';

export function isStrongPassword<T>(property: string) {
  return function (object: T, propertyName: string) {
    registerDecorator({
      name: 'isStrongPassword',
      target: object.constructor,
      propertyName: propertyName,
      constraints: [property],
      validator: {
        validate(password: string) {
          const errorMessages = getPasswordStrengthErrors(password);

          if (!errorMessages) {
            return true;
          }

          return errorMessages.length === 0;
        },
        defaultMessage: ({ value }) => {
          if (typeof value !== 'string') {
            throw new Error('Value is not a string');
          }

          const passwordStrengthErrors = getPasswordStrengthErrors(value);

          if (typeof passwordStrengthErrors === 'string') {
            return passwordStrengthErrors;
          }

          return `${property}: ${passwordStrengthErrors.join(' ')}"`;
        },
      },
    });
  };
}
