import { TypeOrmModule } from '@nestjs/typeorm';

import { seeder } from 'nestjs-seeder';

import { UserSeeder } from 'seeds/user.seeder';

import { typeormSettings } from 'settings/typeorm.settings';

import { User } from 'domain/entities/user.entity';

seeder({
  imports: [TypeOrmModule.forFeature([User]), TypeOrmModule.forRoot(typeormSettings)],
}).run([UserSeeder]);
