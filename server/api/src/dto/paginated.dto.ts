import { ApiProperty } from '@nestjs/swagger';

export class PaginatedDto<TData> {
  items: TData[];

  @ApiProperty({
    default: {
      totalItems: 1,
      itemCount: 1,
      itemsPerPage: 10,
      totalPages: 1,
      currentPage: 1,
    },
  })
  meta: {
    totalItems: number;
    itemCount: number;
    itemsPerPage: number;
    totalPages: number;
    currentPage: number;
  };
}
