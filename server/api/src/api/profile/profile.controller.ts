import { ApiBearerAuth, ApiExtraModels, ApiOkResponse, ApiTags, ApiUnauthorizedResponse } from '@nestjs/swagger';
import { Controller, Get, UseGuards, UseInterceptors, ClassSerializerInterceptor, Req } from '@nestjs/common';

import { Request } from 'express';

import { JwtAuthGuard } from 'api/auth/jwt-auth.guard';

import { getWrappedDtoSchema } from 'utils/dto';

import { ProfileService } from './profile.service';

import { PROFILE_SCHEMA_ROOT_KEY } from './profile.constants';

import { ProfileDto } from './dto/profile.dto';

@ApiTags('Profile')
@ApiExtraModels(ProfileDto)
@ApiBearerAuth()
@ApiUnauthorizedResponse()
@UseInterceptors(ClassSerializerInterceptor)
@UseGuards(JwtAuthGuard)
@Controller()
export class ProfileController {
  constructor(private readonly profileService: ProfileService) {}

  @ApiOkResponse(getWrappedDtoSchema(PROFILE_SCHEMA_ROOT_KEY, ProfileDto))
  @Get()
  async findOne(@Req() req: Request) {
    const profile = await this.profileService.find(req.user.id);

    return { [PROFILE_SCHEMA_ROOT_KEY]: profile };
  }
}
