import { ApiProperty } from '@nestjs/swagger';

import { UserRole, UserStatus } from 'domain/enum/user.enum';
import { UserModel } from 'domain/models/user.model';

import { ApiPropertyUserRole, ApiPropertyUserStatus } from 'decorators/swagger/user';

export class ProfileDto implements Omit<UserModel, 'password'> {
  @ApiProperty()
  id: number;

  @ApiProperty()
  firstName: string;

  @ApiProperty()
  lastName: string;

  @ApiProperty()
  email: string;

  @ApiPropertyUserRole()
  role: UserRole;

  @ApiPropertyUserStatus()
  status: UserStatus;

  @ApiProperty({ default: null })
  avatarUrl: string;
}
