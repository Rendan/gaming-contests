import { ApiProperty } from '@nestjs/swagger';

import { IsEmail, IsNotEmpty } from 'class-validator';

import { isMatch } from 'decorators/validation/is-match.decorator';
import { isStrongPassword } from 'decorators/validation/is-strong-password.decorator';

export class SignUpDto {
  @ApiProperty()
  @IsNotEmpty()
  firstName: string;

  @ApiProperty()
  @IsNotEmpty()
  lastName: string;

  @ApiProperty()
  @IsEmail()
  email: string;

  @ApiProperty()
  @IsNotEmpty()
  @isStrongPassword('password')
  password: string;

  @ApiProperty()
  @IsNotEmpty()
  @isMatch('password')
  passwordConfirmation: string;
}
