import { Injectable, UnauthorizedException } from '@nestjs/common';
import { JwtService, JwtSignOptions } from '@nestjs/jwt';

import { ConfigService } from 'services/config.service';

import { Request, Response } from 'express';
import * as bcrypt from 'bcrypt';
import { omit } from 'ramda';

import { UserRole, UserStatus } from 'domain/enum/user.enum';
import { UserModel } from 'domain/models/user.model';

import { UsersService } from 'api/users/users.service';

import { SignUpDto } from './dto/sign-up.dto';

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService,
    private configService: ConfigService,
  ) {}

  async validateUser(email: string, password: string) {
    const user = await this.usersService.findOneByEmail(email);

    if (!user) {
      return null;
    }

    const hashedPassword = bcrypt.compareSync(password, user.password);

    if (user && hashedPassword) {
      return user;
    }

    return null;
  }

  async signIn(res: Response, req: Request) {
    const { user } = req;

    if (!user) {
      throw new UnauthorizedException();
    }

    const serializedUser = this.serializeUser(user);

    const accessToken = this.jwtService.sign(serializedUser);

    const refreshToken = this.jwtService.sign(serializedUser, this.getRefreshTokenOptions());

    await this.usersService.saveRefreshToken(user.id, refreshToken);

    res.cookie('refreshToken', refreshToken, { httpOnly: true });
    res.json({ accessToken });

    return res.end();
  }

  async signUp(signUpDto: SignUpDto) {
    await this.usersService.create({
      ...signUpDto,
      role: UserRole.GUEST,
      status: UserStatus.ACTIVE,
    });
  }

  private getRefreshTokenOptions() {
    const options: JwtSignOptions = {
      secret: this.configService.getRefreshTokenSecret(),
    };

    const expiration: string = this.configService.getRefreshTokenExpirationTime();

    if (expiration) {
      options.expiresIn = expiration;
    }

    return options;
  }

  async removeRefreshToken(user: UserModel) {
    await this.usersService.removeRefreshToken(user.id);
  }

  async refreshToken(req: Request) {
    // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
    const refreshToken = req.cookies?.refreshToken as string;

    const payload = this.jwtService.decode(refreshToken) as UserModel;

    if (!payload) {
      throw new UnauthorizedException();
    }

    const user = await this.usersService.findOne(payload.id);

    if (!user) {
      throw new UnauthorizedException();
    }

    await this.jwtService.verifyAsync(refreshToken, this.getRefreshTokenOptions());

    if (user.refreshToken !== refreshToken) {
      throw new UnauthorizedException();
    }

    const accessToken = this.jwtService.sign(this.serializeUser(user));

    return {
      accessToken,
    };
  }

  private serializeUser(user: UserModel) {
    const omittedKeys: Array<keyof UserModel> = ['password', 'refreshToken'];

    return omit(omittedKeys, user);
  }
}
