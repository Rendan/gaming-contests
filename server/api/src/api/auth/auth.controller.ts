import { Controller, Post, UseGuards, Body, HttpCode, Res, Req } from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiBody,
  ApiConflictResponse,
  ApiCookieAuth,
  ApiCreatedResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';

import { Request, Response } from 'express';

import { UserModel } from 'domain/models/user.model';

import { AuthService } from './auth.service';

import { LocalAuthGuard } from './local-auth.guard';
import { JwtAuthGuard } from './jwt-auth.guard';

import { AccessTokenDto } from './dto/access-token.dto';
import { CredentialsDto } from './dto/credentials.dto';
import { SignUpDto } from './dto/sign-up.dto';

@ApiTags('Authorization')
@Controller()
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @ApiOperation({ description: 'Sign in to the system' })
  @ApiOkResponse({ type: AccessTokenDto })
  @ApiBody({ type: CredentialsDto })
  @ApiUnauthorizedResponse()
  @UseGuards(LocalAuthGuard)
  @Post('sign-in')
  async signIn(@Res() res: Response, @Req() req: Request) {
    return this.authService.signIn(res, req);
  }

  @ApiOperation({ description: 'Register the new user' })
  @ApiCreatedResponse()
  @ApiConflictResponse()
  @Post('sign-up')
  @HttpCode(200)
  signUp(@Body() signUpDto: SignUpDto) {
    return this.authService.signUp(signUpDto);
  }

  @ApiOperation({ description: 'Logout from the system' })
  @ApiOkResponse()
  @ApiBearerAuth()
  @ApiUnauthorizedResponse()
  @UseGuards(JwtAuthGuard)
  @Post('sign-out')
  @HttpCode(200)
  signOut(@Req() req: Request) {
    const { user } = req;

    return this.authService.removeRefreshToken(user as UserModel);
  }

  @ApiOperation({ description: 'Refresh access token' })
  @ApiOkResponse({ type: AccessTokenDto })
  @ApiCookieAuth()
  @ApiUnauthorizedResponse()
  @Post('refresh')
  @HttpCode(200)
  refresh(@Req() req: Request) {
    return this.authService.refreshToken(req);
  }
}
