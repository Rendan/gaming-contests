import { Injectable, ConflictException, NotFoundException, ForbiddenException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';

import { paginate, Pagination, IPaginationOptions } from 'nestjs-typeorm-paginate';
import { FindConditions, Repository } from 'typeorm';

import { User } from 'domain/entities/user.entity';
import { UserMessages } from 'domain/messages/user.messages';

import { encryptPassword } from 'utils/password';
import { getILikeWhereFindOptions } from 'utils/typeorm';
import { omitNullProperties } from 'utils/object';

import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { UserDto } from './dto/user.dto';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private repository: Repository<User>,
  ) {}

  async create(createUserDto: CreateUserDto) {
    const user = await this.findOneByEmail(createUserDto.email);

    if (user) throw new ConflictException(UserMessages.AlreadyExistsByEmail);

    const hashedPassword = encryptPassword(createUserDto.password);
    const newUser = this.repository.create({
      ...createUserDto,
      password: hashedPassword,
    });
    await this.repository.save(newUser);

    return newUser;
  }

  findAll(
    options: IPaginationOptions,
    searchOptions: FindConditions<User>,
    sortOptions: SortOptions<User>,
  ): Promise<Pagination<UserDto>> {
    const { id, role, status, ...restSearchOptions } = searchOptions;

    return paginate<UserDto>(this.repository, options, {
      where: omitNullProperties({
        id,
        role,
        status,
        ...getILikeWhereFindOptions(omitNullProperties(restSearchOptions)),
      }),
      order: { [sortOptions.sortBy]: sortOptions.sortDirection },
    });
  }

  findOne(id: number) {
    return this.repository.findOne({ where: { id } });
  }

  findOneByEmail(email: string) {
    return this.repository.findOne({
      where: { email },
    });
  }

  saveRefreshToken(userId: number, refreshToken: string) {
    return this.repository.update(userId, {
      refreshToken,
    });
  }

  async removeRefreshToken(userId: number) {
    const user = await this.findOne(userId);

    if (!user) {
      throw new ForbiddenException();
    }

    return this.repository.update(
      { id: userId },
      {
        refreshToken: null,
      },
    );
  }

  async update(id: number, updateUserDto: UpdateUserDto) {
    const user = await this.findOne(id);

    if (!user) throw new NotFoundException(UserMessages.NotFoundById);

    await this.repository.update(id, updateUserDto);

    return this.findOne(id);
  }

  async delete(id: number) {
    const user = await this.findOne(id);

    if (!user) throw new NotFoundException(UserMessages.NotFoundById);

    await this.repository.delete({ id });

    return null;
  }
}
