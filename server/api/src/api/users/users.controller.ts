import {
  ApiBearerAuth,
  ApiCreatedResponse,
  ApiExtraModels,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiTags,
  ApiUnauthorizedResponse,
  ApiUnprocessableEntityResponse,
} from '@nestjs/swagger';
import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
  UseInterceptors,
  ClassSerializerInterceptor,
  HttpCode,
  Query,
} from '@nestjs/common';

import { IPaginationOptions } from 'nestjs-typeorm-paginate';

import { UserRole, UserStatus } from 'domain/enum/user.enum';

import { JwtAuthGuard } from 'api/auth/jwt-auth.guard';

import { ApiPaginatedResponse } from 'decorators/paginated-dto.decorator';
import {
  ApiQueryLimit,
  ApiQueryPage,
  ApiOptionalEntityStringProperty,
  ApiOptionalEntityEnumProperty,
  ApiSortBy,
  ApiSortDirection,
} from 'decorators/swagger';

import { getWrappedDtoSchema } from 'utils/dto';

import { UsersService } from './users.service';

import { USERS_SCHEMA_ROOT_KEY, USER_SCHEMA_ROOT_KEY } from './users.constants';
import { SortingEnum } from './enum';

import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { UserDto } from './dto/user.dto';
import { FindUsersQueryDto } from './dto/find-users-query.dto';

@ApiTags('Users')
@ApiExtraModels(UserDto)
@ApiBearerAuth()
@ApiUnauthorizedResponse()
@UseInterceptors(ClassSerializerInterceptor)
@UseGuards(JwtAuthGuard)
@Controller()
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @ApiUnprocessableEntityResponse()
  @ApiCreatedResponse(getWrappedDtoSchema(USER_SCHEMA_ROOT_KEY, UserDto))
  @Post()
  async create(@Body() createUserDto: CreateUserDto) {
    const user = await this.usersService.create(createUserDto);

    return { [USER_SCHEMA_ROOT_KEY]: user };
  }

  @ApiPaginatedResponse(USERS_SCHEMA_ROOT_KEY, UserDto)
  @ApiQueryLimit()
  @ApiQueryPage()
  @ApiOptionalEntityEnumProperty<UserDto>('role', 'UserRole', UserRole, 'Search by user role')
  @ApiOptionalEntityEnumProperty<UserDto>('status', 'UserStatus', UserStatus, 'Search by user status')
  @ApiOptionalEntityStringProperty<UserDto>('id', 'Search by user ID')
  @ApiOptionalEntityStringProperty<UserDto>('lastName', 'Search by user last name')
  @ApiOptionalEntityStringProperty<UserDto>('firstName', 'Search by user first name')
  @ApiOptionalEntityStringProperty<UserDto>('email', 'Search by user email')
  @ApiSortBy<UserDto>(SortingEnum)
  @ApiSortDirection()
  @Get()
  async findAll(@Query() dto: FindUsersQueryDto) {
    const { page, limit, id, email, firstName, lastName, role, status, sortBy, sortDirection } = dto;

    const paginationOptions: IPaginationOptions = {
      page,
      limit,
    };

    const searchOptions = {
      id,
      email,
      firstName,
      lastName,
      role,
      status,
    };

    const sortOptions = {
      sortBy,
      sortDirection,
    };

    const users = await this.usersService.findAll(paginationOptions, searchOptions, sortOptions);

    return { [USERS_SCHEMA_ROOT_KEY]: users };
  }

  @ApiOkResponse(getWrappedDtoSchema(USER_SCHEMA_ROOT_KEY, UserDto))
  @ApiNotFoundResponse()
  @Get(':id')
  async findOne(@Param('id') id: string) {
    const user = await this.usersService.findOne(Number(id));

    return { user };
  }

  @ApiOkResponse(getWrappedDtoSchema(USER_SCHEMA_ROOT_KEY, UserDto))
  @ApiNotFoundResponse()
  @ApiUnprocessableEntityResponse()
  @Patch(':id')
  async update(@Param('id') id: string, @Body() updateUserDto: UpdateUserDto) {
    const user = await this.usersService.update(Number(id), updateUserDto);

    return { user };
  }

  @ApiOkResponse()
  @ApiNotFoundResponse()
  @Delete(':id')
  @HttpCode(200)
  async delete(@Param('id') id: string) {
    await this.usersService.delete(Number(id));
  }
}
