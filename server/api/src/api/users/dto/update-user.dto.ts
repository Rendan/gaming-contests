import { ApiPropertyOptional } from '@nestjs/swagger';

import { IsEnum, IsOptional, IsNotEmpty } from 'class-validator';

import { UserStatus } from 'domain/enum/user.enum';
import { UserRole } from 'domain/enum/user.enum';
import { UserModel } from 'domain/models/user.model';

import { ApiPropertyUserRole, ApiPropertyUserStatus } from 'decorators/swagger/user';

export class UpdateUserDto implements Pick<UserModel, 'firstName' | 'lastName' | 'role' | 'status'> {
  @ApiPropertyOptional()
  @IsOptional()
  @IsNotEmpty()
  firstName: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsNotEmpty()
  lastName: string;

  @ApiPropertyUserRole({ required: false })
  @IsOptional()
  @IsEnum(UserRole)
  role: UserRole;

  @ApiPropertyUserStatus({ required: false })
  @IsOptional()
  @IsEnum(UserStatus)
  status: UserStatus;
}
