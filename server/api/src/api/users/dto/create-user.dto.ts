import { ApiProperty } from '@nestjs/swagger';

import { IsEmail, IsNotEmpty, IsEnum } from 'class-validator';

import { UserStatus } from 'domain/enum/user.enum';
import { UserRole } from 'domain/enum/user.enum';
import { UserModel } from 'domain/models/user.model';

import { isStrongPassword } from 'decorators/validation/is-strong-password.decorator';
import { isMatch } from 'decorators/validation/is-match.decorator';
import { ApiPropertyUserRole, ApiPropertyUserStatus } from 'decorators/swagger/user';

export class CreateUserDto implements Omit<UserModel, 'id' | 'avatarUrl'> {
  @ApiProperty()
  @IsNotEmpty()
  firstName: string;

  @ApiProperty()
  @IsNotEmpty()
  lastName: string;

  @ApiProperty()
  @IsEmail()
  email: string;

  @ApiProperty()
  @IsNotEmpty()
  @isStrongPassword('password')
  password: string;

  @ApiProperty()
  @IsNotEmpty()
  @isMatch('password')
  passwordConfirmation: string;

  @ApiPropertyUserRole()
  @IsEnum(UserRole)
  role: UserRole;

  @ApiPropertyUserStatus()
  @IsEnum(UserStatus)
  status: UserStatus;
}
