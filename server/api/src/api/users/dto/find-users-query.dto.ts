import { Transform } from 'class-transformer';
import { IsEnum, IsNumber, IsOptional, IsString } from 'class-validator';

import { User } from 'domain/entities/user.entity';
import { UserStatus } from 'domain/enum/user.enum';
import { UserRole } from 'domain/enum/user.enum';
import { UserModel } from 'domain/models/user.model';

import { SortDirection } from 'types/enum/sort';

export class FindUsersQueryDto implements Omit<UserModel, 'avatarUrl' | 'password'> {
  @IsOptional()
  @IsNumber()
  @Transform(({ value }) => Number(value))
  id: number = null;

  @IsOptional()
  firstName: string = null;

  @IsOptional()
  lastName: string = null;

  @IsOptional()
  email: string = null;

  @IsOptional()
  @IsEnum(UserRole)
  role: UserRole = null;

  @IsOptional()
  @IsEnum(UserStatus)
  status: UserStatus = null;

  @IsOptional()
  @IsNumber()
  @Transform(({ value }) => Number(value))
  page = 1;

  @IsOptional()
  @IsNumber()
  @Transform(({ value }) => Number(value))
  limit = 30;

  @IsOptional()
  @IsString()
  sortBy: keyof User = 'id';

  @IsOptional()
  @IsEnum(SortDirection)
  sortDirection: SortDirection = SortDirection.DESC;
}
