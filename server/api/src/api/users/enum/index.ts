export enum SortingEnum {
  id = 'id',
  email = 'email',
  firstName = 'firstName',
  lastName = 'lastName',
  status = 'status',
  role = 'role',
}
