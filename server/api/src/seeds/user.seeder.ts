import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';

import { Repository } from 'typeorm';
import { Seeder } from 'nestjs-seeder';

import { User } from 'domain/entities/user.entity';

import { users } from './data/user.seed-data';

@Injectable()
export class UserSeeder implements Seeder {
  constructor(
    @InjectRepository(User)
    private repository: Repository<User>,
  ) {}

  async seed(): Promise<void> {
    await this.repository.save(users);
  }

  async drop(): Promise<void> {
    await this.repository.clear();
  }
}
