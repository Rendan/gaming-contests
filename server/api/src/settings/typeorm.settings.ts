import { TypeOrmModuleOptions } from '@nestjs/typeorm';

export const typeormSettings: TypeOrmModuleOptions = {
  type: 'postgres',
  port: Number(process.env.POSTGRES_PASSWORD),
  host: process.env.POSTGRES_HOST,
  username: process.env.POSTGRES_USER,
  password: process.env.POSTGRES_PASSWORD,
  database: process.env.POSTGRES_DB,
  synchronize: true,
  entities: [__dirname + '../domain/entities/*.entity.ts'],
  autoLoadEntities: true,
};
