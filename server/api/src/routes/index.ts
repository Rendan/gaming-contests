import { Routes } from '@nestjs/core';

import { AuthModule } from 'api/auth/auth.module';
import { ProfileModule } from 'api/profile/profile.module';
import { UsersModule } from 'api/users/users.module';

export const routes: Routes = [
  {
    path: 'users',
    module: UsersModule,
  },
  {
    path: 'auth',
    module: AuthModule,
  },
  {
    path: 'profile',
    module: ProfileModule,
  },
];
