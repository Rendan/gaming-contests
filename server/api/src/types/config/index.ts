export enum EnvironmentVariablesNames {
  REFRESH_TOKEN_SECRET = 'REFRESH_TOKEN_SECRET',
  REFRESH_TOKEN_EXPIRATION = 'REFRESH_TOKEN_EXPIRATION',
  JWT_SECRET = 'JWT_SECRET',
}

export type Config = {
  [EnvironmentVariablesNames.REFRESH_TOKEN_SECRET]: string;
  [EnvironmentVariablesNames.REFRESH_TOKEN_EXPIRATION]: string;
  [EnvironmentVariablesNames.JWT_SECRET]: string;
};
