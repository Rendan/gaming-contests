import { UserModel } from 'domain/models/user.model';

import { SortDirection } from 'types/enum/sort';

declare global {
  declare type Nullable<T> = T | null;

  declare type SortOptions<T> = {
    sortBy: keyof T;
    sortDirection: SortDirection;
  };

  declare namespace Express {
    interface User extends UserModel {
      id: number;
    }
    interface Request {
      user?: UserModel;
    }
  }
}
